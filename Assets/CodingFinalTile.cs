﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CodingFinalTile : MonoBehaviour
{
    public SpriteRenderer sprite;
    public CodingTile[] pathTiles;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisablePathTiles()
    {
        for (int i = 0; i < pathTiles.Length; i++)
        {
            pathTiles[i].FadeOut();
        }
        FadeOut();
    }

    public void FadeOut()
    {
        sprite.DOFade(0f, 0.5f);
        Debug.Log("naber");
    }
}
