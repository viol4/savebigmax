﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Episode2Map : MonoBehaviour
{
	public Collider2D[] colliders;
	public GameObject robotPanelGO;
    bool passingAnotherScene = false;

    public static Episode2Map Instance;
    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	IEnumerator Start () 
	{
		//Application.targetFrameRate = 60;
		yield return FaderController.Instance.unfadeScreen();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void OpenRobotPanel()
	{
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = false;
		}
		robotPanelGO.SetActive(true);
	}

	public void CloseRobotPanel()
	{
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = true;
		}
		robotPanelGO.SetActive(false);
	}

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
