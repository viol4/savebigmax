﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CodingManager : MonoBehaviour
{
    [Header("UI")]
    public GameObject resultPanelGO;
    public GameObject backButtonGO;
    public Image fader;
    [Space]
    [Header("Properties")]
    public float tileSpaceX;
    public float tileSpaceY;
    [Space]

    public CodingLevel currentLevel;
    public GameObject[] allLevels;

    public int levelIndex = 0;
    bool busy = false;
    bool ended = false;
    bool passingAnotherScene;

    public static CodingManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        currentLevel = Instantiate(allLevels[levelIndex]).GetComponent<CodingLevel>();
        CodingCharacter.Instance.SetPosition(currentLevel.startPosition.position);
        yield return FaderController.Instance.unfadeScreen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsBusy()
    {
        return busy;
    }

    public bool HasEnded()
    {
        return ended;
    }

    public void FailGame()
    {
        ended = true;
        fader.DOKill();
        fader.DOFade(0.5f, 0.25f);
        backButtonGO.SetActive(false);
        resultPanelGO.SetActive(true);
    }

    public void PassNextLevel()
    {
        StartCoroutine(PassNextLevelTask());
    }

    IEnumerator PassNextLevelTask()
    {
        busy = true;
        yield return FaderController.Instance.fadeScreen();
        Destroy(currentLevel.gameObject);
        levelIndex++;
        if(levelIndex >= allLevels.Length)
        {
            levelIndex = 0;
        }
        currentLevel = Instantiate(allLevels[levelIndex]).GetComponent<CodingLevel>();
        CodingCharacter.Instance.SetPosition(currentLevel.startPosition.position);
        yield return FaderController.Instance.unfadeScreen();
        busy = false;
    }

    public void OnBackButtonClick()
    {
        
        CodingCharacter.Instance.StopAllCoroutines();
        CodingCharacter.Instance.transform.DOKill();
        FailGame();

    }

    public void RestartLevel()
    {
        if(busy)
        {
            return;
        }
        StartCoroutine(RestartLevelTask());
    }

    IEnumerator RestartLevelTask()
    {
        fader.DOKill();
        fader.DOFade(0f, 0.25f);
        busy = true;
        yield return FaderController.Instance.fadeScreen();
        Destroy(currentLevel.gameObject);
        if(levelIndex >= allLevels.Length)
        {
            levelIndex = 0;
        }
        currentLevel = Instantiate(allLevels[levelIndex]).GetComponent<CodingLevel>();
        CodingCharacter.Instance.SetPosition(currentLevel.startPosition.position);
        resultPanelGO.SetActive(false);
        backButtonGO.SetActive(true);
        yield return FaderController.Instance.unfadeScreen();
        ended = false;
        busy = false;
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}


}
