﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CodingCharacter : MonoBehaviour
{
    public float moveSpeed = 5f;

    CodingLevel currentLevel;
    bool startedToTravel = false;

    public static CodingCharacter Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            StartTravel();
        }
    }

    void OnMouseDown()
    {
        StartTravel();
    }

    

    void StartTravel()
    {
        if(startedToTravel || CodingManager.Instance.HasEnded())
        {
            return;
        }
        startedToTravel = true;
        currentLevel = CodingManager.Instance.currentLevel;
        StartCoroutine(StartTravelTask());
    }


    IEnumerator StartTravelTask()
    {
        yield return transform.DOMove(currentLevel.startTile.transform.position, moveSpeed).SetSpeedBased().WaitForCompletion();
        while(true)
        {
            yield return new WaitForSeconds(0.5f);
            CodingTile ct = Utility.getColliderAt<CodingTile>(transform.position);
            if(ct)
            {
                if(ct.GetActionType() == CodingItemType.Move)
                {
                    if(ct.isObstacleAction)
                    {
                        startedToTravel = false;
                        CodingManager.Instance.FailGame();
                        break;
                    }                    
                    yield return MoveActionTask(ct.GetMoveAction());
                }
                else if(ct.GetActionType() == CodingItemType.Turn)
                {
                    if(ct.isObstacleAction)
                    {
                        startedToTravel = false;
                        CodingManager.Instance.FailGame();
                        break;
                    }
                    yield return TurnActionTask(ct.GetTurnAction());
                }
                else if(ct.GetActionType() == CodingItemType.Obstacle)
                {
                    if(ct.obstacleActionType != ct.GetObstacleAction())
                    {
                        startedToTravel = false;
                        CodingManager.Instance.FailGame();
                        break;
                    }
                    yield return ObstacleActionTask(ct);
                }
                else
                {
                    startedToTravel = false;
                    CodingManager.Instance.FailGame();
                    break;
                }
            }
            else
            {
                CodingFinalTile cft = Utility.getColliderAt<CodingFinalTile>(transform.position);
                if(cft)
                {
                    if(CodingManager.Instance.currentLevel.IsCorrectTile(cft))
                    {
                        yield return new WaitForSeconds(1f);
                        startedToTravel = false;
                        CodingManager.Instance.PassNextLevel();
                        break;
                    }
                    else
                    {
                        Debug.Log("wrong tree");
                        yield return new WaitForSeconds(0.5f);
                        cft.DisablePathTiles();
                        SetPosition(CodingManager.Instance.currentLevel.startPosition.position);
                        CodingManager.Instance.currentLevel.ClearAllActions();
                        startedToTravel = false;
                        break;
                    }
                }
                else
                {
                    startedToTravel = false;
                    CodingManager.Instance.FailGame();
                    break;
                }
                
            }
            
        }
        
    }

    IEnumerator MoveActionTask(MoveActionType moveAction)
    {
        if(moveAction == MoveActionType.Up)
        {
            yield return transform.DOMoveY(CodingManager.Instance.tileSpaceY, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(moveAction == MoveActionType.Down)
        {
            yield return transform.DOMoveY(-CodingManager.Instance.tileSpaceY, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(moveAction == MoveActionType.Right)
        {
            yield return transform.DOMoveX(CodingManager.Instance.tileSpaceX, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(moveAction == MoveActionType.Left)
        {
            yield return transform.DOMoveX(-CodingManager.Instance.tileSpaceX, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
    }

    IEnumerator TurnActionTask(TurnActionType turnAction)
    {
        if(turnAction == TurnActionType.RightToUp || turnAction == TurnActionType.LeftToUp)
        {
            yield return transform.DOMoveY(CodingManager.Instance.tileSpaceY, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(turnAction == TurnActionType.RightToDown|| turnAction == TurnActionType.LeftToDown)
        {
            yield return transform.DOMoveY(-CodingManager.Instance.tileSpaceY, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(turnAction == TurnActionType.UpToRight || turnAction == TurnActionType.DownToRight)
        {
            yield return transform.DOMoveX(CodingManager.Instance.tileSpaceX, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
        else if(turnAction == TurnActionType.UpToLeft || turnAction == TurnActionType.DownToLeft)
        {
            yield return transform.DOMoveX(-CodingManager.Instance.tileSpaceX, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
        }
    }

    IEnumerator ObstacleActionTask(CodingTile tile)
    {
        if(tile.GetObstacleAction() == ObstacleActionType.Fly)
        {
            yield return transform.DOMoveY(1f, 0.3f).SetRelative().WaitForCompletion();
            yield return new WaitForSeconds(0.3f);
            yield return transform.DOMove(tile.occupationTile.transform.position + new Vector3(0f, 1f, 0f), moveSpeed).SetSpeedBased().WaitForCompletion();
            yield return transform.DOMove(tile.finalTile.transform.position + new Vector3(0f, 1f, 0f), moveSpeed).SetSpeedBased().WaitForCompletion();
            yield return new WaitForSeconds(0.3f);
            yield return transform.DOMoveY(-1f, 0.3f).SetRelative().WaitForCompletion();
        }
        else if(tile.GetObstacleAction() == ObstacleActionType.Cut)
        {
            Vector3 distance = tile.occupationTile.transform.position - transform.position;
            distance /= 2f;
            yield return transform.DOMove(distance, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
            for (int i = 0; i < 3; i++)
            {
                yield return tile.occupationTile.obstacleSprite.transform.DOPunchPosition(Vector2.one * 0.1f, 0.5f).WaitForCompletion();
                yield return new WaitForSeconds(0.25f);
            }
            tile.occupationTile.obstacleSprite.gameObject.SetActive(false);
            yield return transform.DOMove(distance, moveSpeed).SetSpeedBased().SetRelative().WaitForCompletion();
            yield return transform.DOMove(tile.finalTile.transform.position, moveSpeed).SetSpeedBased().WaitForCompletion();
            tile.occupationTile.occupiedByObstacle = false;
            tile.isObstacleAction = false;
        }
    }

    public void SetPosition(Vector3 pos)
    {
        transform.DOKill();
        transform.position = pos;
    }
}
