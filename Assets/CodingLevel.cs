﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodingLevel : MonoBehaviour
{
    
    public Transform tilesParent;
    public CodingTile startTile;
    public Transform startPosition;

    public CodingFinalTile correctFinalTile;

    CodingTile[] tiles;
    void Awake()
    {
        List<CodingTile> tileList = new List<CodingTile>();
        for (int i = 0; i < tilesParent.childCount; i++)
        {
            if(tilesParent.GetChild(i).GetComponent<CodingTile>())
            {
                tileList.Add(tilesParent.GetChild(i).GetComponent<CodingTile>());
            }
        }
        tiles = tileList.ToArray();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearAllActions()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].ResetAction();
        }
    }

    public bool IsCorrectTile(CodingFinalTile tile)
    {
        return tile == correctFinalTile;
    }
}
