﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeOptionManager : MonoBehaviour
{
    public Transform[] options;
    public Sprite[] moveSprites;
    public Sprite[] turnSprites;
    public Sprite[] obstacleSprites;
    public GameObject codeOptionsParent;

    MoveActionType[] moveActionButtonIndices;
    TurnActionType[] turnActionButtonIndices;

    CodingItemType currentAction;

    public CodingTile currentTile;

    public static CodeOptionManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DisableAllOptions()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].gameObject.SetActive(false);
        }
    }

    public void CloseOptionPanel()
    {
        codeOptionsParent.SetActive(false);
    }


    public void OnClickOption(int index)
    {
        if(currentAction == CodingItemType.Move)
        {
            currentTile.SetAction(moveActionButtonIndices[index]);
            CloseOptionPanel();

        }
        else if(currentAction == CodingItemType.Turn)
        {
            currentTile.SetAction(turnActionButtonIndices[index]);
            CloseOptionPanel();
        }
        
    }

    public void SetOptions(CodingTile ct, List<MoveActionType> moveActions)
    {
        currentAction = CodingItemType.Move;
        if(moveActions.Count == 1)
        {
            ct.SetAction(moveActions[0]);
        }
        else
        {
            currentTile = ct;
            moveActionButtonIndices = new MoveActionType[4];
            DisableAllOptions();
            for (int i = 0; i < moveActions.Count; i++)
            {
                if(moveActions[i] == MoveActionType.Up)
                {
                    options[0].gameObject.SetActive(true);
                    options[0].GetChild(1).GetComponent<SpriteRenderer>().sprite = moveSprites[0];
                    options[0].GetChild(1).eulerAngles = new Vector3(0f, 0f, 90f);
                    moveActionButtonIndices[0] = MoveActionType.Up;
                }
                else if(moveActions[i] == MoveActionType.Down)
                {
                    options[1].gameObject.SetActive(true);
                    options[1].GetChild(1).GetComponent<SpriteRenderer>().sprite = moveSprites[1];
                    options[1].GetChild(1).eulerAngles = new Vector3(0f, 0f, 270f);
                    moveActionButtonIndices[1] = MoveActionType.Down;
                }
                else if(moveActions[i] == MoveActionType.Left)
                {
                    options[2].gameObject.SetActive(true);
                    options[2].GetChild(1).GetComponent<SpriteRenderer>().sprite = moveSprites[2];
                    options[2].GetChild(1).eulerAngles = new Vector3(0f, 0f, 180f);
                    moveActionButtonIndices[2] = MoveActionType.Left;
                }
                else if(moveActions[i] == MoveActionType.Right)
                {
                    options[3].gameObject.SetActive(true);
                    options[3].GetChild(1).GetComponent<SpriteRenderer>().sprite = moveSprites[3];
                    options[3].GetChild(1).eulerAngles = new Vector3(0f, 0f, 0f);
                    moveActionButtonIndices[3] = MoveActionType.Right;
                }
                
                
            }
            codeOptionsParent.SetActive(true);
            codeOptionsParent.transform.position = ct.transform.position;

        }
    }

    public void SetOptions(CodingTile ct, List<TurnActionType> turnActions)
    {
        currentAction = CodingItemType.Turn;
        currentTile = ct;
        turnActionButtonIndices = new TurnActionType[4];
        DisableAllOptions();
        int offset = (int)turnActions[0] < 5 ? 2 : 0;
        for (int i = 0; i < turnActions.Count; i++)
        {
            options[i + offset].gameObject.SetActive(true);
            options[i + offset].GetChild(1).GetComponent<SpriteRenderer>().sprite = turnSprites[(int)turnActions[i] - 1];
            options[i + offset].GetChild(1).eulerAngles = new Vector3(0f, 0f, 0f);
            turnActionButtonIndices[i + offset] = turnActions[i];
        }
        codeOptionsParent.SetActive(true);
        codeOptionsParent.transform.position = ct.transform.position;
    }


}

public enum MoveActionType
{
    None, Up, Down, Left, Right
}

public enum TurnActionType
{
    None, UpToRight, UpToLeft, DownToRight, DownToLeft, LeftToUp, LeftToDown, RightToUp, RightToDown
}

public enum ObstacleActionType
{
    None, Fly, Cut
}
