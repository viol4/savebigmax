﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CodingItemUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject itemFakePrefab;
    public CodingItemType itemType;
    public int obstacleIndex = -1;

    Transform currentFake;
    bool holding = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(holding)
        {
            currentFake.position = Utility.mouseToWorldPosition();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(holding || CodingManager.Instance.IsBusy() || CodingManager.Instance.HasEnded())
        {
            return;
        }
        holding = true;
        currentFake = Instantiate(itemFakePrefab).transform;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(!holding)
        {
            return;
        }
        holding = false;
        CodingTile ct = Utility.getColliderAt<CodingTile>(Utility.mouseToWorldPosition());
        if(ct && !ct.occupiedByObstacle)
        {
            if(itemType == CodingItemType.Move)
            {
                ct.CheckForAvailableMoveActions();
            }
            else if(itemType == CodingItemType.Turn)
            {
                ct.CheckForAvailableTurnActions();
            }
            else if(itemType == CodingItemType.Obstacle)
            {
                ct.SetObstacleAction(obstacleIndex);
            }
        }
        Destroy(currentFake.gameObject);

    }


}

public enum CodingItemType
{
    None, Move, Turn, Obstacle
}
