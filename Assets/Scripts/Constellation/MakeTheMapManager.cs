﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MakeTheMapManager : MonoBehaviour
{
    public PikaConstellation[] constellations;

    bool passingAnotherScene = false;
    int[] completedArray;

    public static MakeTheMapManager Instance;
    void Awake()
    {
        Instance = this;
        if(!PlayerPrefs.HasKey("sbm_constellations_completed"))
        {
            PlayerPrefs.SetString("sbm_constellations_completed", "0000000000");
            completedArray = Utility.CharArrayToIntArray("0000000000");
        }
        else
        {
            completedArray = Utility.CharArrayToIntArray(PlayerPrefs.GetString("sbm_constellations_completed"));
        }
    }

	// Use this for initialization
	IEnumerator Start () 
	{
		//Application.targetFrameRate = 60;
        if(ConstellationManager.constellationId >= 0)
        {
            Vector3 pos = constellations[ConstellationManager.constellationId].transform.position;
            pos.z = -10f;
            DomeController.Instance.transform.position = pos;
            if(ConstellationManager.completed)
            {
                completedArray[ConstellationManager.constellationId] = 1;
                SaveCompletedArray();
            }
        }
		yield return FaderController.Instance.unfadeScreen();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

    void SaveCompletedArray()
    {
        PlayerPrefs.SetString("sbm_constellations_completed", Utility.IntArrayToCharArray(completedArray));
    }

    public bool IsConstellationIdCompleted(int id)
    {
        return completedArray[id] == 1;
    }

    public void GoToConstellation(int id)
    {
        if(passingAnotherScene)
        {
            return;
        }
        ConstellationManager.constellationId = id;
        LoadScene("Constellation");
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
