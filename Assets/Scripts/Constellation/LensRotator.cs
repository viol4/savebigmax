﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensRotator : MonoBehaviour
{
    public Transform target;
    public Transform[] parallaxes;
    public float speed;

    bool holding = false;
    bool keyboardHolding = false;
    Vector3 lastMousePos;
    // Start is called before the first frame update
    void Start()
    {
        lastMousePos = Input.mousePosition;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1) && !holding)
        {
            holding = true;
        }
        else if(Input.GetMouseButtonUp(1) && holding)
        {
            holding = false;
        }


        if(holding)
        {
            Vector3 deltaPos = lastMousePos - Input.mousePosition;
            target.Translate(deltaPos * Time.deltaTime * speed);
            Vector3 pos = target.position;
            pos.x = Mathf.Clamp(pos.x, -36.5f, 55.4f);
            pos.y = Mathf.Clamp(pos.y, -22f, 12.72f);
            target.position = pos;
            if(!(Mathf.Approximately(pos.x, -36.5f) || Mathf.Approximately(pos.x, 55.4f)
                || Mathf.Approximately(pos.y, -22f) || Mathf.Approximately(pos.y, 12.72f)))
            {
                for (int i = 0; i < parallaxes.Length; i++)
                {
                    parallaxes[i].Translate(deltaPos * Time.deltaTime * speed * 0.95f);
                }
            }
            
        }
        else 
        {
            Vector3 deltaPos = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            target.Translate(deltaPos * Time.deltaTime * speed * 15f);
            Vector3 pos = target.position;
            pos.x = Mathf.Clamp(pos.x, -36.5f, 55.4f);
            pos.y = Mathf.Clamp(pos.y, -22f, 12.72f);
            target.position = pos;
            if(!(Mathf.Approximately(pos.x, -36.5f) || Mathf.Approximately(pos.x, 55.4f)
                || Mathf.Approximately(pos.y, -22f) || Mathf.Approximately(pos.y, 12.72f)))
            {
                for (int i = 0; i < parallaxes.Length; i++)
                {
                    parallaxes[i].Translate(deltaPos * Time.deltaTime * speed * 15f * 0.95f);
                }
            }
        }
        lastMousePos = Input.mousePosition;
    }
}
