﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StarColliderTrigger : MonoBehaviour
{
    Constellation constellation;
    // Start is called before the first frame update
    void Start()
    {
        constellation = GetComponentInParent<Constellation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        constellation.OnMouseDownStar(GetComponent<MTMStarMenu>());
    }

    void OnMouseUp()
    {
        constellation.OnMouseUpStar(GetComponent<MTMStarMenu>());
    }
}
