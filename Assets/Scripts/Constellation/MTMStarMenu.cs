﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTMStarMenu : MonoBehaviour
{
    public int starId;
    public int[] neighbours;
    public bool isMenu = true;


    PikaConstellation currentConstellation;
    // Start is called before the first frame update
    void Start()
    {
        if(isMenu)
        {
            currentConstellation = GetComponentInParent<PikaConstellation>();
            ConnectAllNeighbours();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ConnectAllNeighbours()
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            currentConstellation.ConnectStars(starId, neighbours[i]);
        }
    }

    
}

public class StarConnection
{
    public int fromId;
    public int toId;

    public StarConnection(int f, int t)
    {
        fromId = f;
        toId = t;
    }
}
