﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class PikaConstellation : MonoBehaviour
{
    public int constellationId = 0;
    public MTMStarMenu[] starMenus;
    public TextMeshPro nameTag;
    public Transform lineParent;
    public LineRenderer lineRenderer;

    List<StarConnection> starConnections;
    bool hidden = true;


    // Start is called before the first frame update
    void Awake()
    {
        starConnections = new List<StarConnection>();
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.3f);
        HideLines();
        if(Vector3.Distance(DomeController.Instance.transform.position, transform.position) < 12.5f)
        {
            hidden = false;
            if(MakeTheMapManager.Instance.IsConstellationIdCompleted(constellationId))
            {
                ShowLines();
                nameTag.DOKill();
                nameTag.DOFade(0.5f, 0.5f);
            }
        }
        else 
        {
            hidden = true;
            
            if(MakeTheMapManager.Instance.IsConstellationIdCompleted(constellationId))
            {
                HideLines();
                nameTag.DOKill();
                nameTag.DOFade(0f, 0.5f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(hidden && Vector3.Distance(DomeController.Instance.transform.position, transform.position) < 12.5f)
        {
            hidden = false;
            
            if(MakeTheMapManager.Instance.IsConstellationIdCompleted(constellationId))
            {
                ShowLines();
                nameTag.DOKill();
                nameTag.DOFade(0.5f, 0.5f);
            }
        }
        else if(!hidden && Vector3.Distance(DomeController.Instance.transform.position, transform.position) >= 12.5f)
        {
            hidden = true;
            
            if(MakeTheMapManager.Instance.IsConstellationIdCompleted(constellationId))
            {
                HideLines();
                nameTag.DOKill();
                nameTag.DOFade(0f, 0.5f);
            }
        }
    }

    void ShowLines()
    {
        for (int i = 0; i < lineParent.childCount; i++)
        {
            lineParent.GetChild(i).GetComponent<LineRenderer>().DOKill();
            lineParent.GetChild(i).GetComponent<LineRenderer>().DOColor(new Color2(new Color(1f, 1f, 1f, 0f), new Color(1f, 1f, 1f, 0f)), new Color2(new Color(1f, 1f, 1f, 0.2f), new Color(1f, 1f, 1f, 0.2f)), 0.5f);
        }
    }

    void HideLines()
    {
        for (int i = 0; i < lineParent.childCount; i++)
        {
            lineParent.GetChild(i).GetComponent<LineRenderer>().DOKill();
            lineParent.GetChild(i).GetComponent<LineRenderer>().DOColor(new Color2(new Color(1f, 1f, 1f, 0.2f), new Color(1f, 1f, 1f, 0.2f)), new Color2(new Color(1f, 1f, 1f, 0f), new Color(1f, 1f, 1f, 0f)), 0.5f);
        }
    }

    public bool Contains(int fromId, int toId)
    {
        for (int i = 0; i < starConnections.Count; i++)
        {
            if((starConnections[i].fromId == fromId && starConnections[i].toId == toId) || (starConnections[i].fromId == toId && starConnections[i].toId == fromId))
            {
                return true;
            }
        }
        return false;
    }

    public MTMStarMenu GetStarById(int id)
    {
        for (int i = 0; i < starMenus.Length; i++)
        {
            if(starMenus[i].starId == id)
            {
                return starMenus[i];
            }
        }
        return null;
    }

    public void ConnectStars(int id1, int id2)
    {
        if(!Contains(id1, id2))
        {
            MTMStarMenu star1 = null;
            MTMStarMenu star2 = null;
            for (int i = 0; i < starMenus.Length; i++)
            {
                if(starMenus[i].starId == id1)
                {
                    star1 = starMenus[i];
                }
                else if(starMenus[i].starId == id2)
                {
                    star2 = starMenus[i];
                }
            }
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, star1.transform.position);
            lineRenderer.SetPosition(1, star2.transform.position);
            GameObject lineGO = Instantiate(lineRenderer.gameObject, lineParent);
            lineRenderer.positionCount = 0;

        }
    }

    void OnMouseUpAsButton()
    {
        MakeTheMapManager.Instance.GoToConstellation(constellationId);
    }

    
}
