﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Constellation : MonoBehaviour
{
    public MTMStarMenu[] starMenus;
    public LineRenderer lineRenderer;
    public Transform lineParent;
    public TextMeshPro nameTag;
    public TextMeshPro[] letters;


    bool holding = false;
    bool finished = false;
    int currentStarIndex = 0;

    List<StarConnection> starConnections;
    MTMStarMenu currentStar = null;

    void Awake()
    {
        starConnections = new List<StarConnection>();
    }
    // Start is called before the first frame update
    void Start()
    {
        SetLetters();
    }

    // Update is called once per frame
    void Update()
    {
        if(finished)
        {
            return;
        }
        if(currentStar != null)
        {
            lineRenderer.SetPosition(1, Utility.mouseToWorldPosition());
            MTMStarMenu otherStar = Utility.getColliderAt<MTMStarMenu>(Utility.mouseToWorldPosition());
            if(otherStar)
            {
                for (int i = 0; i < currentStar.neighbours.Length; i++)
                {
                    if(currentStar.neighbours[i] == otherStar.starId && !Contains(currentStar.starId, otherStar.starId))
                    {
                        starConnections.Add(new StarConnection(currentStar.starId, otherStar.starId));
                        lineRenderer.positionCount = 2;
                        lineRenderer.SetPosition(0, currentStar.transform.position);
                        lineRenderer.SetPosition(1, otherStar.transform.position);
                        GameObject lineGO = Instantiate(lineRenderer.gameObject, lineParent);
                        currentStarIndex++;
                        if(currentStarIndex == starMenus.Length - 1)
                        {
                            if(otherStar.neighbours.Length > 0)
                            {
                                lineRenderer.positionCount = 2;
                                lineRenderer.SetPosition(0, otherStar.transform.position);
                                lineRenderer.SetPosition(1, GetStarById(otherStar.neighbours[0]).transform.position);
                                lineGO = Instantiate(lineRenderer.gameObject, lineParent);
                            }
                            currentStar = null;
                            nameTag.DOFade(1f, 1f);
                            finished = true;
                            lineRenderer.positionCount = 0;
                            ConstellationManager.Instance.FinishGame();
                            break;

                        }
                        else
                        {
                            OnMouseDownStar(otherStar);
                            break;
                        }
                        
                        
                        
                    }
                }
            }
        }
        
        if(Input.GetMouseButtonUp(0) && currentStar)
        {
            OnMouseUpStar(currentStar);
        }
    }

    void SetLetters()
    {
        if(Random.value >= 0.5f)
        {
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].text = characters[i].ToString();
            }
        }
        else
        {
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].text = i.ToString();
            }
        }
    }

    public void OnMouseDownStar(MTMStarMenu star)
    {
        if(finished)
        {
            return;
        }
        if(star.starId == currentStarIndex)
        {
            currentStar = star;
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, star.transform.position);
        }
        
    }

    public void OnMouseUpStar(MTMStarMenu star)
    {
        if(finished)
        {
            return;
        }
        if(currentStar == star)
        {
            currentStar = null;
            lineRenderer.positionCount = 0;
        }
        
    }

    public bool IsFinished()
    {
        // for (int i = 0; i < starMenus.Length; i++)
        // {
        //     for (int j = 0; j < starMenus[i].neighbours.Length; j++)
        //     {
        //         if(!Contains(starMenus[i].starId, starMenus[i].neighbours[j]))
        //         {
        //             return false;
        //         }
        //     }
        // }
        return finished;
    }

    public bool Contains(int fromId, int toId)
    {
        for (int i = 0; i < starConnections.Count; i++)
        {
            if((starConnections[i].fromId == fromId && starConnections[i].toId == toId) || (starConnections[i].fromId == toId && starConnections[i].toId == fromId))
            {
                return true;
            }
        }
        return false;
    }

    public MTMStarMenu GetStarById(int id)
    {
        for (int i = 0; i < starMenus.Length; i++)
        {
            if(starMenus[i].starId == id)
            {
                return starMenus[i];
            }
        }
        return null;
    }



    
}
