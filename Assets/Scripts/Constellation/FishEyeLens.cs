﻿using UnityEngine;

public class FishEyeLens : MonoBehaviour
{
    Material mat;
    void Start()
    {
        Shader shader = Shader.Find("Hidden/ZubrVR/DomeProjection");
        if (shader != null)
        {
            mat = new Material(shader);
        }
    }

    void Update()
    {
        
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, mat);
    }
}