﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConstellationManager : MonoBehaviour
{
    public Constellation[] constellations;

    bool passingAnotherScene;
    Constellation currentConstellation;


    public static int constellationId = -1;
    public static bool completed = false;
    // Start is called before the first frame update
    public static ConstellationManager Instance;
    void Awake()
    {
        Instance = this;
        for (int i = 0; i < constellations.Length; i++)
        {
            if(i == constellationId)
            {
                currentConstellation = constellations[i];
                continue;
            }
            Destroy(constellations[i].gameObject);
        }
        currentConstellation.gameObject.SetActive(true);
    }
    IEnumerator Start()
    {
        yield return FaderController.Instance.unfadeScreen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FinishGame()
    {
        StartCoroutine(FinishGameTask());
    }

    IEnumerator FinishGameTask()
    {
        yield return new WaitForSeconds(2f);
        LoadScene("MakeTheMap");
        completed = true;
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
        completed = false;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
