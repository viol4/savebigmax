﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AvoidAsteroidManager : MonoBehaviour
{
    public int level = 1;
    public int point = 0;

    public Text pointText;
    public Text starText;
    public Image fader;
    public GameObject resultPanelGO;
    public GameObject backButtonGO;

    int totalStars = 0;
    bool sessionActive = true;
    bool passingAnotherScene;


    public static AvoidAsteroidManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return FaderController.Instance.unfadeScreen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FinishGame()
    {
        ChangMeteorGenerator.Instance.Deactivate();
        ChangRobot.Instance.StopAllCoroutines();
        ChangRobot.Instance.MakeEnergyFull();
        ChangRobot.Instance.ResetRobot();
        sessionActive = false;
        fader.DOKill();
        fader.DOFade(0.5f, 0.6f);
        backButtonGO.SetActive(false);
        resultPanelGO.SetActive(true);
        int star = 0;
        if(point >= 30)
        {
            star = 3 * (point / 30);
        }
        else if(point >= 20)
        {
            star = 2;
        }
        else if(point >= 10)
        {
            star = 1;
        }
        totalStars += star;
        starText.text = totalStars.ToString();
        starText.transform.parent.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        StartCoroutine(RestartGameTask());
    }

    IEnumerator RestartGameTask()
    {
        point = 0;
        level = 1;
        starText.transform.parent.gameObject.SetActive(false);
        pointText.text = point.ToString();
        resultPanelGO.SetActive(false);
        backButtonGO.SetActive(true);
        fader.DOKill();
        yield return fader.DOFade(0f, 0.3f).WaitForCompletion();
        sessionActive = true;
        ChangRobot.Instance.MakeEnergyFull();
        ChangMeteorGenerator.Instance.Activate();
    }

    public bool IsSessionActive()
    {
        return sessionActive;
    }

    public void GainPoint()
    {
        point++;
        pointText.text = point.ToString();
        CheckForLevel();
    }

    void CheckForLevel()
    {
        if(level == 1 && point >= 1)
        {
            level = 2;
        }
        else if(level == 2 && point >= 3)
        {
            level = 3;
        }
        else if(level == 3 && point >= 6)
        {
            level = 4;
        }
        else if(level == 4 && point >= 9)
        {
            level = 5;
        }
        else if(level == 5 && point >= 12)
        {
            level = 6;
        }
        else if(level == 6 && point >= 15)
        {
            level = 7;
        }
        else if(level == 7 && point >= 18)
        {
            level = 8;
        }
        else if(level == 8 && point >= 22)
        {
            level = 9;
        }
        else if(level == 9 && point >= 26)
        {
            level = 10;
        }
        else if(level == 10 && point >= 31)
        {
            level = 11;
        }
    }

    public int GetMaxAsteroidCount()
    {
        if(level <= 3)
        {
            return 1;
        }
        else if(level <= 6)
        {
            return 2;
        }
        else if(level <= 8)
        {
            return 3;
        }
        else if(level <= 10)
        {
            return 4;
        }
        else
        {
            return 5;
        }  
    }

    public void OnBackButtonClick()
    {
        FinishGame();
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
