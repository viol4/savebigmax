﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ChangRobot : MonoBehaviour
{
    public float min_X = -9f;
    public float max_X = 9f;

    public SpriteRenderer shieldSprite;
    public Collider2D shieldCollider;
    public Image energyBar;

    public Gradient energyGradient;

    bool canActivateShield = true;

    public static ChangRobot Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if(AvoidAsteroidManager.Instance.IsSessionActive() && Input.GetMouseButtonDown(0))
        {
            ActivateShield();
        }
        energyBar.color = energyGradient.Evaluate(energyBar.fillAmount);
    }

    void Move()
    {
        if(!AvoidAsteroidManager.Instance.IsSessionActive())
        {
            return;
        }
        float x = Utility.mouseToWorldPosition().x;
        x = Mathf.Clamp(x, min_X, max_X);
        transform.position = new Vector3(x, transform.position.y, 0f);
    }

    

    public void MakeEnergyFull()
    {
        energyBar.DOKill();
        energyBar.DOFillAmount(1f, 0.3f);
        shieldSprite.DOKill();
        shieldSprite.DOFade(0f, 0.3f);
    }

    public void ResetRobot()
    {
        shieldSprite.DOKill();
        shieldSprite.DOFade(0f, 0.3f);
        shieldCollider.enabled = false;
        energyBar.DOKill();
        canActivateShield = true;
    }


    public void ActivateShield()
    {
        if(canActivateShield)
        {
            canActivateShield = false;
            StartCoroutine(ActivateShieldTask());
        }
    }

    IEnumerator ActivateShieldTask()
    {
        shieldSprite.DOFade(1f, 0.3f);
        shieldCollider.enabled = true;
        shieldSprite.DOFade(0.75f, 0.7f).SetLoops(-1, LoopType.Yoyo);
        energyBar.DOKill();
        yield return energyBar.DOFillAmount(0f, 5f).WaitForCompletion();
        shieldSprite.DOKill();
        shieldSprite.DOFade(0f, 0.3f);
        shieldCollider.enabled = false;
        energyBar.DOKill();
        yield return energyBar.DOFillAmount(1f, 7f).WaitForCompletion();
        canActivateShield = true;
    }
}
