﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChangEnemyMeteor : MonoBehaviour
{
    public GameObject explosionPrefab;
    public SpriteRenderer meteorSprite;
    public float rotateSpeed;
    public float moveSpeed;
    public float colliderThresoldTime = 1f;

    float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
        transform.DOMoveY(-20f, moveSpeed).SetRelative().SetSpeedBased();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < -6f)
        {
            DestroyMeteor();
            return;
        }
        meteorSprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
        if(!GetComponent<CircleCollider2D>().enabled)
        {
            timer += Time.deltaTime;
            if(timer > colliderThresoldTime)
            {
                GetComponent<CircleCollider2D>().enabled = true;
            }
        }
    }

    public void DestroyMeteor()
    {
        ChangMeteorGenerator.Instance.RemoveMeteor(this);
        AvoidAsteroidManager.Instance.GainPoint();
        GameObject explosionGO = Instantiate(explosionPrefab);
        explosionGO.transform.position = transform.position;
        Destroy(explosionGO, 1.5f);
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name.Equals("shieldSprite"))
        {
            //ChangMeteorGenerator.Instance.RemoveMeteor(this);
            DestroyMeteor();
            //ProtectCottonManager.Instance.FinishGame();
        }
        else if(other.name.Equals("flower") && !ChangRobot.Instance.shieldCollider.enabled)
        {
            AvoidAsteroidManager.Instance.FinishGame();
        }
        
    }
}
