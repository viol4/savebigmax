﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RunnerCornian : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveUpDownTask());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator MoveUpDownTask()
    {
        float distance = Random.Range(0.07f, 0.15f);
        float multiplier = (Random.value > 0.5f ? 1f : -1f);
        float duration = Random.Range(0.4f, 0.6f);
        while(true)
        {
            yield return transform.DOMoveY(distance * multiplier, duration).SetRelative().SetEase(Ease.InOutSine).WaitForCompletion();
            yield return transform.DOMoveY(distance * -multiplier, duration).SetRelative().SetEase(Ease.InOutSine).WaitForCompletion();
        }
    }
}
