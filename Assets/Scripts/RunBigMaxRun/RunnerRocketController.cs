﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RunnerRocketController : MonoBehaviour
{
    public SpriteRenderer rocketSprite;
    public Transform movementPoint;
    public SpriteRenderer[] shieldSprites;
    public Image fuelGauge;
    [Space]
    public float minY;
    public float maxY;
    public float moveSpeed;
    [Space]
    int shieldPoints = 3;
    float fuel = 10000f;

    Vector3 firstRotation;

    public static RunnerRocketController Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        firstRotation = rocketSprite.transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        MovePoint();
        MoveRocket();
        SpendFuel();
        // if(Input.GetKeyDown(KeyCode.Space))
        // {
        //     ShakeByHit();
        //     LoseShields();
        // }
        // else if(Input.GetKeyDown(KeyCode.K))
        // {
        //     GainShields();
        // }


    }

    void SpendFuel()
    {
        if(RBRManager.Instance.IsSessionActive())
        {
            fuel -= 3f;
            fuelGauge.fillAmount = Mathf.Lerp(fuelGauge.fillAmount, fuel / 10000f, 10f * Time.deltaTime);
            if(fuel <= 0)
            {
                fuel = 0f;
                RBRManager.Instance.SetResultText("RUN OUT OF FUEL");
                RBRManager.Instance.FinishGame();
            }
        }
    }

    void MovePoint()
    {
        if(RBRManager.Instance.IsSessionActive())
        {
            movementPoint.Translate(Vector3.up * Input.GetAxisRaw("Vertical") * Time.deltaTime * moveSpeed);
            Vector3 pos = movementPoint.position;
            pos.y = Mathf.Clamp(pos.y, minY, maxY);
            movementPoint.position = pos;
        }
    }

    void CheckForDistanceToCornians()
    {
        if(shieldPoints == 3)
        {
            movementPoint.position = new Vector3(0f, movementPoint.position.y, 0f);
        }
        else if(shieldPoints == 2)
        {
            movementPoint.position = new Vector3(-1.6f, movementPoint.position.y, 0f);
        }
        else if(shieldPoints == 1)
        {
            movementPoint.position = new Vector3(-3.2f, movementPoint.position.y, 0f);
        }
        else if(shieldPoints == 0)
        {
            movementPoint.position = new Vector3(-4.8f, movementPoint.position.y, 0f);
        }
        
    }

    void ShakeByHit()
    {
        rocketSprite.transform.DOKill();
        rocketSprite.transform.localEulerAngles = firstRotation;
        rocketSprite.transform.DOPunchRotation(Vector3.forward * 10f, 1f, 15, 1f);
    }

    void LoseShields()
    {
        shieldPoints--;
        shieldSprites[shieldPoints].DOKill();
        shieldSprites[shieldPoints].DOFade(0f, 0.3f);
        CheckForDistanceToCornians();
    }
    
    void GainShields()
    {
        if(shieldPoints < 3)
        {
            shieldSprites[shieldPoints].DOKill();
            shieldSprites[shieldPoints].DOFade(1f, 0.3f);
            shieldPoints++;
            CheckForDistanceToCornians();
        }
    }
    
    void GainFuel(int amount)
    {
        fuel += amount;
        fuel = Mathf.Clamp(fuel, 0f, 10000f);
    }

    void MoveRocket()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(movementPoint.position.x, movementPoint.position.y, 0f), 5f * Time.deltaTime);
    }

    public void FillShields()
    {
        for (int i = 0; i < shieldSprites.Length; i++)
        {
            shieldSprites[i].DOKill();
            shieldSprites[i].DOFade(1f, 0.3f);
        }
        shieldPoints = 3;
        CheckForDistanceToCornians();
    }

    public void FillFuel()
    {
        fuel = 10000;
    }

    public int GetShieldPoint()
    {
        return shieldPoints;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<RunnerPocketWatch>())
        {
            other.GetComponent<RunnerPocketWatch>().DestroyWatch();
            RBRManager.Instance.GainPoint();
        }
        else if(other.GetComponent<RunnerFuel>())
        {
            other.GetComponent<RunnerFuel>().DestroyFuel();
            GainFuel(2000);
        }
        else if(other.GetComponent<RunnerShield>())
        {
            other.GetComponent<RunnerShield>().DestroyShield();
            GainShields();
        }
        else if(other.GetComponent<RBRMeteor>())
        {
            if(other.name.Contains("planetMeteor"))
            {
                other.GetComponent<RBRMeteor>().StopPlanet();
                RBRManager.Instance.SetResultText("YOU HITTED PLANET");
                RBRManager.Instance.FinishGame();
            }
            else
            {
                other.GetComponent<RBRMeteor>().DestroyMeteor();
                if(shieldPoints > 0)
                {
                    ShakeByHit();
                    LoseShields();
                }
                else
                {
                    RBRManager.Instance.SetResultText("RUN OUT OF SHIELDS");
                    RBRManager.Instance.FinishGame();
                }
            }
            
            
        }
    }
}
