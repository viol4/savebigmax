﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RBRManager : MonoBehaviour
{
    public Text pointText;
    public Text starText;
    public Image pointImage;
    public Text resultText;
    public GameObject resultPanelGO;
    public GameObject starPanelGO;
    public GameObject backButtonGO;
    public Image fader;
    public ParticleSystem starParticle;
    public float speedMultiplier = 1f;

    public int point = 0;

    bool passingAnotherScene;
    bool sessionActive = true;
    int totalStars = 0;
    Tweener speedTween;
    float speedTimer = 0f;

    public static RBRManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return FaderController.Instance.unfadeScreen();
    }

    // Update is called once per frame
    void Update()
    {
        if(sessionActive)
        {
            var main = starParticle.main;
            main.simulationSpeed = speedMultiplier;
            speedTimer += Time.deltaTime;
            if(speedTimer >= 10f)
            {
                speedTimer = 0f;
                speedTween.Kill();
                speedTween = DOTween.To(()=> speedMultiplier, x=> speedMultiplier = x, speedMultiplier + 0.05f, 1);
                
            }
        }
        
    }

    public void SetResultText(string text)
    {
        resultText.text = text;
    }

    public void RestartGame()
    {
        speedTimer = 0f;
        point = 0;
        pointText.text = point.ToString();
        sessionActive = true;
        fader.DOKill();
        fader.DOFade(0f, 0.3f);
        resultPanelGO.SetActive(false);
        starPanelGO.SetActive(false);
        backButtonGO.SetActive(true);
        RunnerRocketController.Instance.FillShields();
        RunnerRocketController.Instance.FillFuel();
        RunnerMeteorSpawner.Instance.Activate();
        speedTween.Kill();
        speedTween = DOTween.To(()=> speedMultiplier, x=> speedMultiplier = x, 1f, 1);
        
    }

    public void OnBackButtonClick()
    {
        FinishGame();
    }

    public void FinishGame()
    {
        speedTimer = 0f;
        sessionActive = false;
        fader.DOKill();
        fader.DOFade(0.5f, 0.3f);
        resultPanelGO.SetActive(true);
        starPanelGO.SetActive(true);
        backButtonGO.SetActive(false);

        int star = 0;
        if(point >= 20)
        {
            star = 3 * (point / 20);
        }
        else if(point >= 10)
        {
            star = 2;
        }
        else if(point >= 5)
        {
            star = 1;
        }
        totalStars += star;
        starText.text = totalStars.ToString();

        RunnerMeteorSpawner.Instance.Deactivate();
        RunnerFuel[] fuels = FindObjectsOfType<RunnerFuel>();
        for (int i = 0; i < fuels.Length; i++)
        {
            fuels[i].DestroyFuel();
        }
        RunnerShield[] shields = FindObjectsOfType<RunnerShield>();
        for (int i = 0; i < shields.Length; i++)
        {
            shields[i].DestroyShield();
        }
        RunnerPocketWatch[] watches = FindObjectsOfType<RunnerPocketWatch>();
        for (int i = 0; i < watches.Length; i++)
        {
            watches[i].DestroyWatch();
        }
    }

    

    public bool IsSessionActive()
    {
        return sessionActive;
    }

    public void GainPoint()
    {
        point++;
        pointText.text = point.ToString();
        pointImage.transform.DOKill();
        pointImage.transform.DOPunchScale(Vector3.one * 0.3f, 0.7f);
        pointText.transform.DOPunchScale(Vector3.one * 0.3f, 0.7f);
    }

    public float GetTimeLimitForPoints()
    {
        if(point > 100)
        {
            return 0.5f;
        }
        else if(point > 50)
        {
            return 1f;
        }
        else if(point > 25)
        {
            return 1.5f;
        }
        else 
        {
            return 2f;
        }
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
