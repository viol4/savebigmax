﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RBRMeteor : MonoBehaviour
{
    public GameObject explosionPrefab;
    public SpriteRenderer meteorSprite;
    public float rotateSpeed;
    public float moveSpeed;
    public float colliderThresoldTime = 1f;

    public TrailRenderer[] trails;
    
    float[] trailTimes;

    float timer = 0f;
    //Tween moveTween;
    // Start is called before the first frame update
    void Start()
    {
        trailTimes = new float[trails.Length];
        for (int i = 0; i < trails.Length; i++)
        {
            trailTimes[i] = trails[i].time;
        }
       // moveTween = transform.DOMoveX(-40f, moveSpeed).SetRelative().SetSpeedBased();
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * moveSpeed * RBRManager.Instance.speedMultiplier);
        meteorSprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
        for (int i = 0; i < trails.Length; i++)
        {
            trails[i].time = trailTimes[i] / RBRManager.Instance.speedMultiplier;
        }
        // if(!GetComponent<CircleCollider2D>().enabled)
        // {
        //     timer += Time.deltaTime;
        //     if(timer > colliderThresoldTime)
        //     {
        //         GetComponent<CircleCollider2D>().enabled = true;
        //     }
        // }

        if(transform.position.x < -20f)
        {
            DestroyMeteor();
        }
    }

    public void DestroyMeteor()
    {
        RunnerMeteorSpawner.Instance.RemoveMeteor(this);
        GameObject explosionGO = Instantiate(explosionPrefab);
        explosionGO.transform.position = transform.position;
        Destroy(explosionGO, 1.5f);
        Destroy(gameObject);
    }

    public void StopPlanet()
    {
        transform.DOKill();
    }


}
