﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RunnerShield : MonoBehaviour
{
    public SpriteRenderer shieldSprite;
    public float rotateSpeed;
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        //transform.DOMoveX(-40f, moveSpeed).SetRelative().SetSpeedBased();
        Destroy(gameObject, 30f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * moveSpeed * RBRManager.Instance.speedMultiplier);
        shieldSprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
    }

    public void DestroyShield()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        shieldSprite.sortingOrder = 600;
        shieldSprite.DOFade(0f, 0.3f);
        shieldSprite.transform.DOScale(shieldSprite.transform.localScale * 2f, 0.3f);
        Destroy(gameObject, 0.3f);
    }
}
