﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RunnerPocketWatch : MonoBehaviour
{
    public SpriteRenderer pocketSprite;
    public float rotateSpeed;
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
        Destroy(gameObject, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * moveSpeed * RBRManager.Instance.speedMultiplier);
        pocketSprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
    }

    public void DestroyWatch()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        pocketSprite.sortingOrder = 600;
        pocketSprite.DOFade(0f, 0.3f);
        pocketSprite.transform.DOScale(pocketSprite.transform.localScale * 2f, 0.3f);
        Destroy(gameObject, 0.3f);
    }
}
