﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerMeteorSpawner : MonoBehaviour
{
    public GameObject meteorPrefab;
    public GameObject rabbitMeteorPrefab;
    public GameObject stableMeteorPrefab;
    public GameObject planetMeteorPrefab;
    [Space]
    public GameObject pocketWatchPrefab;
    public GameObject fuelPrefab;
    public GameObject shieldPrefab;
    [Space]
    public Transform meteorParent;
    public Transform pocketWatchParent;
    public Transform fuelParent;
    public Transform shieldParent;
    public Transform meteorSpawnPlaces;
    public Transform planetSpawnPlaces;

    List<GameObject> prefabList;
    List<RBRMeteor> activeMeteors;
    Vector3[] spawnPositions;
    int lastSpawnIndex = -1;
    float timer = 0f;
    float timerLimit = 0.1f;
    float pocketTimer = 0f;
    float fuelTimer = 0f;
    float shieldTimer = 0f;
    bool active = false;

    public static RunnerMeteorSpawner Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        spawnPositions = new Vector3[meteorSpawnPlaces.childCount];
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            spawnPositions[i] = meteorSpawnPlaces.GetChild(i).position;
        }
        prefabList = new List<GameObject>();
        activeMeteors = new List<RBRMeteor>();
        pocketTimer = 10f;
        Activate();
    }

    // Update is called once per frame
    void Update()
    {
        if(!active)
        {
            return;
        }
        timer += Time.deltaTime;
        if(timer >= timerLimit)
        {
            timer = 0f;
            timerLimit = RBRManager.Instance.GetTimeLimitForPoints();
            SpawnRandomMeteor();
        }

        pocketTimer += Time.deltaTime;
        if(pocketTimer > 3f)
        {
            pocketTimer = 0f;
            SpawnRandomPocketWatch();
        } 

        fuelTimer += Time.deltaTime;
        if(fuelTimer > 5f)
        {
            fuelTimer = 0f;
            SpawnRandomFuel();
        }   

        if(RunnerRocketController.Instance.GetShieldPoint() < 3)
        {
            shieldTimer += Time.deltaTime;
            if(shieldTimer > 6f)
            {
                shieldTimer = 0f;
                SpawnRandomShield();
            }  
        }
               
    }

    void PreparePrefabList()
    {
        prefabList.Clear();
        if(RBRManager.Instance.point < 10)
        {
            prefabList.Add(stableMeteorPrefab);
        }
        else if(RBRManager.Instance.point < 20)
        {
            prefabList.Add(stableMeteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(planetMeteorPrefab);
        }
        else if(RBRManager.Instance.point < 30)
        {
            prefabList.Add(stableMeteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(planetMeteorPrefab);
        }
        else if(RBRManager.Instance.point < 40)
        {
            prefabList.Add(stableMeteorPrefab);
            prefabList.Add(stableMeteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(planetMeteorPrefab);
        }
        else if(RBRManager.Instance.point < 50)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(planetMeteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(RBRManager.Instance.point < 100)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(planetMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(RBRManager.Instance.point >= 100)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(stableMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(planetMeteorPrefab);
        }
        
        Utility.ShuffleList(ref prefabList);
    }

    GameObject GetRandomPrefab()
    {
        generateNewMeteor:
        if(prefabList.Count == 0)
        {
            PreparePrefabList();
        }
        int index = Random.Range(0, prefabList.Count);
        GameObject result = prefabList[index];
        if(result.GetComponent<RBRMeteor>().name.Contains("planetMeteor"))
        {
            for (int i = 0; i < activeMeteors.Count; i++)
            {
                if(activeMeteors[i].name.Contains("planetMeteor"))
                {
                    prefabList.RemoveAt(index);
                    goto generateNewMeteor;
                }
            }
        }
        prefabList.RemoveAt(index);
        return result;

    }

    
    
    Vector3 GetRandomPosition()
    {
        int index = -1;
        do
        {
            index = Random.Range(0, spawnPositions.Length);
        }
        while(index == lastSpawnIndex);
        lastSpawnIndex = index;
        return spawnPositions[index];
    }

    Vector3 GetRandomPlanetPosition()
    {
        int index = Random.Range(0, planetSpawnPlaces.childCount);
        return planetSpawnPlaces.GetChild(index).position;
    }

    public void SpawnRandomMeteor()
    {
        RBRMeteor cem = Instantiate(GetRandomPrefab(), meteorParent).GetComponent<RBRMeteor>();
        if(cem.name.Contains("planetMeteor"))
        {
            cem.transform.position = GetRandomPlanetPosition();
        }
        else
        {
            cem.transform.position = GetRandomPosition();
        }
        cem.gameObject.SetActive(true);
        activeMeteors.Add(cem);
    }

    public void SpawnRandomPocketWatch()
    {
        RunnerPocketWatch rpw = Instantiate(pocketWatchPrefab, pocketWatchParent).GetComponent<RunnerPocketWatch>();
        rpw.transform.position = GetRandomPosition();
        rpw.gameObject.SetActive(true);
        //activeMeteors.Add(cem);
    }

    public void SpawnRandomFuel()
    {
        RunnerFuel rpw = Instantiate(fuelPrefab, fuelParent).GetComponent<RunnerFuel>();
        rpw.transform.position = GetRandomPosition();
        rpw.gameObject.SetActive(true);
        //activeMeteors.Add(cem);
    }

    public void SpawnRandomShield()
    {
        RunnerShield rpw = Instantiate(shieldPrefab, shieldParent).GetComponent<RunnerShield>();
        rpw.transform.position = GetRandomPosition();
        rpw.gameObject.SetActive(true);
        //activeMeteors.Add(cem);
    }

    public void RemoveMeteor(RBRMeteor cem)
    {
        activeMeteors.Remove(cem);
    }

    public void Activate()
    {
        active = true;
        PreparePrefabList();
    }

    public void Deactivate()
    {
        active = false;
        for (int i = 0; i < activeMeteors.Count; i++)
        {
            activeMeteors[i].DestroyMeteor();
        }
        activeMeteors.Clear();
    }


}
