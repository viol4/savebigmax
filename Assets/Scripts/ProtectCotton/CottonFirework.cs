﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CottonFirework : MonoBehaviour
{
    public SpriteRenderer fireworkSprite;
    public GameObject explosionPrefab;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDirection(Vector3 dir)
    {
        fireworkSprite.transform.localRotation = Quaternion.Euler(0f, 0f, Utility.angleBetween(Vector3.zero, dir));
        transform.DOMove(dir * 100f, speed).SetSpeedBased().SetRelative();
        Destroy(gameObject, 3f);
    }

    public void DestroyFirework()
    {
        GameObject explosionGO = Instantiate(explosionPrefab);
        explosionGO.transform.position = transform.position;
        Destroy(explosionGO, 3f);
        Destroy(gameObject);
    }

}
