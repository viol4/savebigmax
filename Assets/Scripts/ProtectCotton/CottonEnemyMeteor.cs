﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CottonEnemyMeteor : MonoBehaviour
{
    public GameObject explosionPrefab;
    public SpriteRenderer meteorSprite;
    public float rotateSpeed;
    public float moveSpeed;
    public float colliderThresoldTime = 1f;

    float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
        transform.DOMove(CottonFlower.Instance.transform.position, moveSpeed).SetSpeedBased();
    }

    // Update is called once per frame
    void Update()
    {
        meteorSprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
        if(!GetComponent<CircleCollider2D>().enabled)
        {
            timer += Time.deltaTime;
            if(timer > colliderThresoldTime)
            {
                GetComponent<CircleCollider2D>().enabled = true;
            }
        }
    }

    public void DestroyMeteor()
    {
        GameObject explosionGO = Instantiate(explosionPrefab);
        explosionGO.transform.position = transform.position;
        Destroy(explosionGO, 1.5f);
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<CottonFlower>())
        {
            CottonMeteorSpawner.Instance.RemoveMeteor(this);
            DestroyMeteor();
            ProtectCottonManager.Instance.FinishGame();
        }
        else if(other.GetComponent<CottonShield>())
        {
            CottonMeteorSpawner.Instance.RemoveMeteor(this);
            other.GetComponent<CottonShield>().DisableShield();
            DestroyMeteor();
        }
        else if(other.GetComponent<CottonFirework>())
        {
            CottonMeteorSpawner.Instance.RemoveMeteor(this);
            other.GetComponent<CottonFirework>().DestroyFirework();
            Destroy(gameObject);
            ProtectCottonManager.Instance.GainPoint();
        }
    }
}
