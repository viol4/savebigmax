﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CottonRobot : MonoBehaviour
{
    public SpriteRenderer robotSprite;
    public GameObject fireworkPrefab;
    public Transform fireworkParent;
    
    Vector3 firstScale;
    // Start is called before the first frame update
    void Start()
    {
        firstScale = robotSprite.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if(ProtectCottonManager.Instance.IsSessionActive() && Input.GetMouseButtonDown(0))
        {
            SpawnFirework();
        }
    }

    void SpawnFirework()
    {
        robotSprite.transform.DOKill();
        robotSprite.transform.localScale = firstScale;
        robotSprite.transform.DOPunchScale(Vector3.one * 0.1f, 0.3f);
        CottonFirework cf = Instantiate(fireworkPrefab, fireworkParent).GetComponent<CottonFirework>();
        cf.gameObject.SetActive(true);
        cf.SetDirection((Utility.mouseToWorldPosition().normalized));

    }
}
