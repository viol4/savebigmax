﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottonMeteorSpawner : MonoBehaviour
{
    public GameObject meteorPrefab;
    public GameObject rabbitMeteorPrefab;
    public Transform meteorParent;
    public Transform meteorSpawnPlaces;

    List<GameObject> prefabList;
    List<CottonEnemyMeteor> activeMeteors;
    Vector3[] spawnPositions;
    int lastSpawnIndex = -1;
    float timer = 0f;
    float timerLimit = 0.1f;
    bool active = false;

    public static CottonMeteorSpawner Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        spawnPositions = new Vector3[meteorSpawnPlaces.childCount];
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            spawnPositions[i] = meteorSpawnPlaces.GetChild(i).position;
        }
        prefabList = new List<GameObject>();
        activeMeteors = new List<CottonEnemyMeteor>();
        Activate();
    }

    // Update is called once per frame
    void Update()
    {
        if(!active)
        {
            return;
        }
        timer += Time.deltaTime;
        if(activeMeteors.Count < ProtectCottonManager.Instance.GetMaxAsteroidCount() && timer >= timerLimit)
        {
            timer = 0f;
            timerLimit = Random.Range(0.3f, 1f);
            SpawnRandomMeteor();
        }
    }

    void PreparePrefabList()
    {
        prefabList.Clear();
        if(ProtectCottonManager.Instance.level == 1)
        {
            prefabList.Add(meteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 2)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 3)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 4)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 5)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 6)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 7)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 8)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 9)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level == 10)
        {
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(meteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
            prefabList.Add(rabbitMeteorPrefab);
        }
        else if(ProtectCottonManager.Instance.level > 10)
        {
            int rabbitCount = Random.Range(3, 6);
            for (int i = 0; i < rabbitCount; i++)
            {
                prefabList.Add(rabbitMeteorPrefab);
            }
            for (int i = 0; i < 8 - rabbitCount; i++)
            {
                prefabList.Add(meteorPrefab);
            }
        }
        Utility.ShuffleList(ref prefabList);
    }

    GameObject GetRandomPrefab()
    {
        if(prefabList.Count == 0)
        {
            PreparePrefabList();
        }
        int index = Random.Range(0, prefabList.Count);
        GameObject result = prefabList[index];
        prefabList.RemoveAt(index);
        return result;

    }
    
    Vector3 GetRandomPosition()
    {
        int index = -1;
        do
        {
            index = Random.Range(0, spawnPositions.Length);
        }
        while(index == lastSpawnIndex);
        lastSpawnIndex = index;
        return spawnPositions[index];
    }

    public void SpawnRandomMeteor()
    {
        CottonEnemyMeteor cem = Instantiate(GetRandomPrefab(), meteorParent).GetComponent<CottonEnemyMeteor>();
        cem.transform.position = GetRandomPosition();
        cem.gameObject.SetActive(true);
        activeMeteors.Add(cem);
    }

    public void RemoveMeteor(CottonEnemyMeteor cem)
    {
        activeMeteors.Remove(cem);
    }

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
        for (int i = 0; i < activeMeteors.Count; i++)
        {
            activeMeteors[i].DestroyMeteor();
        }
        activeMeteors.Clear();
    }
}
