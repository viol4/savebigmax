﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottonShield : MonoBehaviour
{
    PolygonCollider2D collider2d;
    SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        collider2d = GetComponent<PolygonCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisableShield()
    {
        collider2d.enabled = false;
        spriteRenderer.enabled = false;
    }

    public void EnableShield()
    {
        collider2d.enabled = true;
        spriteRenderer.enabled = true;
    }
}
