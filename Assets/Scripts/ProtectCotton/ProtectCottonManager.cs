﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProtectCottonManager : MonoBehaviour
{
    public int level = 1;
    public int point = 0;
    public int totalStars = 0;

    public Text starText;
    public Text successText;
    public Image fader;
    public GameObject resultPanelGO;
    public GameObject backButtonGO;

    public CottonShield[] shields;

    public Texture2D cursorTexture;

    bool sessionActive = false;
    bool passingAnotherScene;

    public static ProtectCottonManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
        yield return FaderController.Instance.unfadeScreen();
        sessionActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GainPoint()
    {
        point++;
        successText.text = point.ToString();
        CheckForLevel();
    }
    
    public bool IsSessionActive()
    {
        return sessionActive;
    }

    public void OnBackButtonClick()
    {
        FinishGame();
    }

    public void FinishGame()
    {
        backButtonGO.SetActive(false);
        CottonMeteorSpawner.Instance.Deactivate();
        sessionActive = false;
        fader.DOKill();
        fader.DOFade(0.5f, 0.6f);
        resultPanelGO.SetActive(true);
        int star = 0;
        if(point >= 30)
        {
            star = 3 * (point / 30);
        }
        else if(point >= 20)
        {
            star = 2;
        }
        else if(point >= 10)
        {
            star = 1;
        }
        totalStars += star;
        starText.text = totalStars.ToString();
    }

    void CheckForLevel()
    {
        if(level == 1 && point >= 1)
        {
            level = 2;
        }
        else if(level == 2 && point >= 3)
        {
            level = 3;
        }
        else if(level == 3 && point >= 6)
        {
            level = 4;
        }
        else if(level == 4 && point >= 9)
        {
            level = 5;
        }
        else if(level == 5 && point >= 12)
        {
            level = 6;
        }
        else if(level == 6 && point >= 15)
        {
            level = 7;
        }
        else if(level == 7 && point >= 18)
        {
            level = 8;
        }
        else if(level == 8 && point >= 22)
        {
            level = 9;
        }
        else if(level == 9 && point >= 26)
        {
            level = 10;
        }
        else if(level == 10 && point >= 31)
        {
            level = 11;
        }
    }

    public void RestartGame()
    {
        StartCoroutine(RestartGameTask());
    }

    IEnumerator RestartGameTask()
    {
        point = 0;
        level = 1;
        successText.text = point.ToString();
        backButtonGO.SetActive(true);
        resultPanelGO.SetActive(false);
        fader.DOKill();
        yield return fader.DOFade(0f, 0.3f).WaitForCompletion();
        for (int i = 0; i < shields.Length; i++)
        {
            shields[i].EnableShield();
            yield return new WaitForSeconds(0.4f);
        }
        sessionActive = true;
        CottonMeteorSpawner.Instance.Activate();
    }

    public int GetMaxAsteroidCount()
    {
        if(level <= 3)
        {
            return 1;
        }
        else if(level <= 6)
        {
            return 2;
        }
        else if(level <= 8)
        {
            return 3;
        }
        else if(level <= 10)
        {
            return 4;
        }
        else
        {
            return 5;
        }  
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
