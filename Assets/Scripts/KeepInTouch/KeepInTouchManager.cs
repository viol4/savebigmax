﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KeepInTouchManager : MonoBehaviour
{
	public GameObject resultPanelGO;
	public GameObject backButtonGO;
	public GameObject simonPanelGO;
	public GameObject successPanelGO;
	public Image fader;
	[Space]
	public RectTransform threePanel;
	public RectTransform fourPanel;
	public RectTransform fivePanel;
	public Text starText;
	public Text successText;
	public Sprite[] iconSprites;

    bool passingAnotherScene;
	bool showingSequence = false;
	bool failShow = false;
	bool resetted = false;

	int[] currentArray;
	int arrayIndex = 0;

	int successCounter = 0;
	int totalStar = 0;

	int panelIndex = 0;
	int variableCount = 3;
	int length = 2;

	public static KeepInTouchManager Instance;
	void Awake()
	{
		Instance = this;
	}

    // Start is called before the first frame update
    IEnumerator Start()
    {
        //HideAllPanels();
		//yield return new WaitForSeconds(0.5f);
		SetButtonIcons();
		StartToShowSequence();
		yield return FaderController.Instance.unfadeScreen();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
		{
			StartToShowSequence();
		}
    }

	void UpdateSuccessPanel()
	{
		successText.text = successCounter.ToString();
	}

	void SetButtonIcons()
	{
		List<int> indexList = new List<int>();
		for (int i = 0; i < iconSprites.Length; i++)
		{
			indexList.Add(i);
		}
		fourPanel.gameObject.SetActive(true);
		fivePanel.gameObject.SetActive(true);
		Utility.ShuffleList(ref indexList);
		SimonButton[] allSimonButtons = FindObjectsOfType<SimonButton>();
		fourPanel.gameObject.SetActive(false);
		fivePanel.gameObject.SetActive(false);
		for (int i = 0; i < allSimonButtons.Length; i++)
		{
			allSimonButtons[i].ChangeIcon(iconSprites[indexList[allSimonButtons[i].index]]);
		}
	}

	SimonButton[] GetCurrentSimonButtons()
	{
		SimonButton[] simonButtons = new SimonButton[variableCount];
		if(variableCount == 3)
		{
			for (int i = 0; i < threePanel.childCount; i++)
			{
				simonButtons[i] = threePanel.GetChild(i).GetComponent<SimonButton>();
			}
		}
		else if(variableCount == 4)
		{
			for (int i = 0; i < fourPanel.childCount; i++)
			{
				simonButtons[i] = fourPanel.GetChild(i).GetComponent<SimonButton>();
			}
		}
		else if(variableCount == 5)
		{
			for (int i = 0; i < fivePanel.childCount; i++)
			{
				simonButtons[i] = fivePanel.GetChild(i).GetComponent<SimonButton>();
			}
		}
		return simonButtons;
	}

	void CalculateVariables()
	{
		
		if(successCounter < 6)
		{
			panelIndex = 0;
			variableCount = 3;
			if(successCounter < 3)
			{
				length = 2;
			}
			else if(successCounter >= 3)
			{
				length = 3;
			}
		}
		else if(successCounter < 15)
		{
			panelIndex = 1;
			variableCount = 4;
			if(successCounter < 9)
			{
				length = 2;
			}
			else if(successCounter < 12)
			{
				length = 3;
			}
			else if(successCounter >= 12)
			{
				length = 4;
			}
		}
		else if(successCounter >= 15)
		{
			panelIndex = 2;
			variableCount = 5;
			if(successCounter < 18)
			{
				length = 2;
			}
			else if(successCounter < 21)
			{
				length = 3;
			}
			else if(successCounter < 24)
			{
				length = 4;
			}
			else if(successCounter >= 24)
			{
				length = 5;
			}
		}
	}
	
	void HideAllPanels(bool immediately)
	{
		for (int i = 0; i < threePanel.childCount; i++)
		{
			threePanel.GetChild(i).GetComponent<SimonButton>().Hide(immediately);
		}
		threePanel.gameObject.SetActive(false);

		for (int i = 0; i < fourPanel.childCount; i++)
		{
			fourPanel.GetChild(i).GetComponent<SimonButton>().Hide(immediately);
		}
		fourPanel.gameObject.SetActive(false);

		for (int i = 0; i < fivePanel.childCount; i++)
		{
			fivePanel.GetChild(i).GetComponent<SimonButton>().Hide(immediately);
		}
		fivePanel.gameObject.SetActive(false);
	}

	void ShowCurrentPanel()
	{
		ShowPanel(variableCount - 3);
	}

	void CalculateStars()
	{
		int stars = 0;
        if(successCounter >= 20)
        {
            stars = 3 * (successCounter / 20);
        }
        else if(successCounter >= 10)
        {
            stars = 2;
        }
        else if(successCounter >= 5)
        {
            stars = 1;
        }
        totalStar += stars;
		starText.text = totalStar.ToString();
	}

	void ShowPanel(int index)
	{
		if(index == 0)
		{
			threePanel.gameObject.SetActive(true);
			for (int i = 0; i < threePanel.childCount; i++)
            {
                threePanel.GetChild(i).GetComponent<SimonButton>().Show();
            }
		}
		else if(index == 1)
		{
			fourPanel.gameObject.SetActive(true);
			for (int i = 0; i < fourPanel.childCount; i++)
            {
                fourPanel.GetChild(i).GetComponent<SimonButton>().Show();
            }
		}
		else if(index == 2)
		{
			fivePanel.gameObject.SetActive(true);
			for (int i = 0; i < fivePanel.childCount; i++)
            {
                fivePanel.GetChild(i).GetComponent<SimonButton>().Show();
            }
		}
	}

	public bool IsAvailableForInteract()
	{
		return !showingSequence && !failShow;
	}

	public void StopGame()
	{
		CalculateStars();
		successPanelGO.SetActive(false);
		SimonTimeManager.Instance.StopTimer();
		simonPanelGO.SetActive(false);
		backButtonGO.SetActive(false);
		resultPanelGO.SetActive(true);
		fader.DOKill();
		fader.DOFade(0.5f, 0.25f);
	}

	public void StartToShowSequence()
	{
		if(showingSequence)
		{
			return;
		}
		showingSequence = true;
		CalculateVariables();
		currentArray = SimonPatternGenerator.Instance.GetRandomPattern(variableCount, length);
		arrayIndex = 0;
		StartCoroutine(ShowSequenceTask(currentArray));
	}

	IEnumerator ShowSequenceTask(int[] array)
	{
		//ShowCurrentPanel();
		if(successCounter == 0 && resetted)
		{
			resetted = false;
			yield return StageUpAnimTask(0);
		}
		if(successCounter == 6)
		{
			yield return StageUpAnimTask(1);
		}
		else if(successCounter == 15)
		{
			yield return StageUpAnimTask(2);
		}
		// else if(successCounter == 24)
		// {
		// 	yield return StageUpAnimTask(3);
		// }
		
		SimonButton[] simonButtons = GetCurrentSimonButtons();
		yield return new WaitForSeconds(1f);
		SimonTimeManager.Instance.Fill();
		for (int i = 0; i < simonButtons.Length; i++)
		{
			simonButtons[i].OnPointerUp(null);
		}
		yield return new WaitForSeconds(1f);
		for (int i = 0; i < array.Length; i++)
		{
			simonButtons[array[i]].ShineWithBounce();
			yield return new WaitForSeconds(0.3f);
			simonButtons[array[i]].DimWithBounce();
			yield return new WaitForSeconds(0.5f);
		}
		SimonTimeManager.Instance.StartTimer();
		showingSequence = false;
	}

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}

		public int GetVariableCount()
	{
		return variableCount;
	}

	public void TimeIsUp()
	{
		CalculateStars();
		successCounter = 0;
		UpdateSuccessPanel();
		resetted = true;
		failShow = true;
		StartCoroutine(FailAnimTask(currentArray[arrayIndex], -1));
	}

	public void CheckForAnswer(int index)
	{
		if(index == currentArray[arrayIndex])
		{
			arrayIndex++;
			if(arrayIndex == currentArray.Length)
			{
				successCounter++;
				UpdateSuccessPanel();
				SimonTimeManager.Instance.StopTimer();
				StartToShowSequence();
			}
		}
		else
		{
			CalculateStars();
			successCounter = 0;
			UpdateSuccessPanel();
			if(panelIndex > 0)
			{
				resetted = true;
			}
			failShow = true;
			SimonTimeManager.Instance.StopTimer();
			StartCoroutine(FailAnimTask(currentArray[arrayIndex], index));
		}
	}

	IEnumerator StageUpAnimTask(int index)
	{
		yield return new WaitForSeconds(1f);
		HideAllPanels(false);
		yield return new WaitForSeconds(0.3f);
		ShowPanel(index);
		panelIndex = index;
		yield return new WaitForSeconds(0.3f);
	}

	IEnumerator FailAnimTask(int index, int failIndex = -1)
	{
		SimonButton[] simonButtons = GetCurrentSimonButtons();
		if(failIndex >= 0)
		{
			simonButtons[failIndex].OnPointerUp(null);
		}
		yield return simonButtons[index].CorrectAnimTask();
		failShow = false;
		StartToShowSequence();
	}

	public int GetLength()
	{
		return length;
	}
}
