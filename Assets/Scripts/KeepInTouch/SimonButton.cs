﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimonButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image backImage;
    public Image iconImage;
    public int index = 0;
    public Color passiveColor;
    public Color activeColor;
    public Color iconDarkColor;

    bool holding = false;
    // Start is called before the first frame update
    void Start()
    {
        backImage.color = passiveColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator CorrectAnimTask()
    {
        backImage.DOKill();
        iconImage.DOKill();
        backImage.transform.DOKill();
        iconImage.transform.DOKill();
        for (int i = 0; i < 3; i++)
        {
            backImage.transform.DOScale(1.3f, 0.2f);
            iconImage.transform.DOScale(1.3f, 0.2f);
            backImage.DOColor(activeColor, 0.2f);
            iconImage.DOColor(Color.white, 0.2f);
            yield return new WaitForSeconds(0.2f);
            backImage.transform.DOScale(1f, 0.2f);
            iconImage.transform.DOScale(1f, 0.2f);
            backImage.DOColor(passiveColor, 0.2f);
            iconImage.DOColor(iconDarkColor, 0.2f);
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void ShineWithBounce()
    {
        backImage.DOKill();
        iconImage.DOKill();
        backImage.transform.DOKill();
        iconImage.transform.DOKill();
        backImage.DOColor(activeColor, 0.1f);
        iconImage.DOColor(Color.white, 0.1f);
        
        backImage.transform.DOScale(1.3f, 0.1f);
        iconImage.transform.DOScale(1.3f, 0.1f);
    }

    public void DimWithBounce()
    {
        backImage.DOKill();
        iconImage.DOKill();
        backImage.transform.DOKill();
        iconImage.transform.DOKill();
        backImage.DOColor(passiveColor, 0.1f);
        iconImage.DOColor(iconDarkColor, 0.1f);

        backImage.transform.DOScale(1f, 0.1f);
        iconImage.transform.DOScale(1f, 0.1f);
    }

    public void Shine()
    {
        backImage.DOKill();
        iconImage.DOKill();
        backImage.DOColor(activeColor, 0.1f);
        iconImage.DOColor(Color.white, 0.1f);
    }

    public void Dim()
    {
        backImage.DOKill();
        iconImage.DOKill();
        backImage.DOColor(passiveColor, 0.1f);
        iconImage.DOColor(iconDarkColor, 0.1f);
    }

    public void Hide(bool immediately = false)
    {
        backImage.DOKill();
        iconImage.DOKill();

        if(immediately)
        {
            Utility.changeAlphaImage(backImage, 0f);
            Utility.changeAlphaImage(iconImage, 0f);
        }
        else
        {
            backImage.DOFade(0f, 0.3f);
            iconImage.DOFade(0f, 0.3f);
        }
        
    }

    public void Show(bool immediately = false)
    {
        backImage.DOKill();
        iconImage.DOKill();

        if(immediately)
        {
            Utility.changeAlphaImage(backImage, 1f);
            Utility.changeAlphaImage(iconImage, 1f);
        }
        else
        {
            backImage.DOFade(1f, 0.3f);
            iconImage.DOFade(1f, 0.3f);
        }
        Dim();
    }

    public void ChangeIcon(Sprite sprite)
    {
        iconImage.sprite = sprite;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(holding || !KeepInTouchManager.Instance.IsAvailableForInteract())
        {
            return;
        }
        holding = true;
        ShineWithBounce();
        KeepInTouchManager.Instance.CheckForAnswer(index);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(!holding)
        {
            return;
        }
        holding = false;
        DimWithBounce();
    }

    
}
