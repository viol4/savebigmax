﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FunFactManager : MonoBehaviour
{
    public GameObject canvasGO;
    public Image faderImage;
    public Image backImage;
    public Text factText;
    public Image closeButtonImage;

    [Space]
    public Color greenColor;
    public Color orangeColor;
    public Color purpleColor;

    FunFact[] funFacts;

    bool shown = false;

    public static FunFactManager Instance;
    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(canvasGO);
        }
        else
        {
            DestroyImmediate(canvasGO);
            DestroyImmediate(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        funFacts = Resources.LoadAll<FunFact>("FunFacts");
    }

    // Update is called once per frame
    void Update()
    {
        // if(Input.GetKeyDown(KeyCode.Space))
        // {
        //     ShowFact("Mercury");
        // }
        // else if(Input.GetKeyDown(KeyCode.M))
        // {
        //     ShowFact("Sun");
        // }
    }

    public Color GetFactColor(int index)
    {
        if(index == 0)
        {
            return greenColor;
        }
        else if(index == 1)
        {
            return orangeColor;
        }
        else
        {
            return purpleColor;
        }
    }

    public void ShowFact(string factName)
    {
        if(shown)
        {
            return;
        }
        shown = true;
        List<FunFact> funFactList = new List<FunFact>();
        for (int i = 0; i < funFacts.Length; i++)
        {
            if(funFacts[i].factName.Equals(factName) && funFacts[i].episode <= GameManager.Instance.episodeReached)
            {
                funFactList.Add(funFacts[i]);
            }
        }
        if(funFactList.Count > 0)
        {
            int factIndex = PlayerPrefs.GetInt("sbm_factindex_" + factName);
            if(factIndex > GameManager.Instance.episodeReached)
            {
                factIndex = 0;
                PlayerPrefs.SetInt("sbm_factindex_" + factName, 0);
            }
            //factIndex = Mathf.Min(factIndex, funFactList.Count - 1);
            factText.text = funFactList[factIndex].factContent;
            backImage.color = GetFactColor(factIndex);
            ShowMessage();
            PlayerPrefs.SetInt("sbm_factindex_" + factName, factIndex + 1);
        }
    }

    public void ShowMessage()
    {
        
        canvasGO.SetActive(true);
        faderImage.DOKill();
        backImage.DOKill();
        factText.DOKill();
        closeButtonImage.DOKill();
        closeButtonImage.rectTransform.DOKill();

        faderImage.raycastTarget = true;
        faderImage.DOFade(0.5f, 0.5f);
        backImage.DOFade(1f, 0.5f);
        factText.DOFade(1f, 0.5f);
        Utility.changeAlphaImage(closeButtonImage, 1f);
        closeButtonImage.rectTransform.localScale = Vector3.one * 0.5f;
        closeButtonImage.rectTransform.DOScale(1f, 1f).SetEase(Ease.OutElastic);
        closeButtonImage.raycastTarget = true;
    }

    public void HideMessage()
    {
        if(!shown)
        {
            return;
        }
        faderImage.DOKill();
        backImage.DOKill();
        factText.DOKill();
        closeButtonImage.rectTransform.DOKill();
        closeButtonImage.DOKill();
        closeButtonImage.raycastTarget = false;

        faderImage.raycastTarget = false;
        faderImage.DOFade(0f, 0.5f).OnComplete(() => 
        {
            canvasGO.SetActive(false);
            shown = false;
        });
        backImage.DOFade(0f, 0.5f);
        factText.DOFade(0f, 0.5f);
        closeButtonImage.DOFade(0f, 0.5f);
    }
}
