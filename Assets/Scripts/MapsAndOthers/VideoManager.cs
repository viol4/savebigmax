﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public GameObject videoCanvasGO;
    public GraphicRaycaster videoGR;
    public VideoPlayer _videoPlayer;
    public RawImage videoRawImage;
    public Image barBack;
    public Image gaugeBar;
    public Image bigMaxPointer;
    public Text timeText;
    public AudioSource audioSource;
    public RectTransform barRect;
    public RectTransform volumeSliderRect;
    public Slider volumeSlider;

    public Image playButton;
    public Image pauseButton;

    bool sessionActive = false;
    bool barshown = true;
    bool volumeBarShown = false;
    Vector3 lastMousePos;
    float idleMouseTimer = 0f;
    
    public static VideoManager Instance;
    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(videoCanvasGO);
        }
        else
        {
            DestroyImmediate(videoCanvasGO);
            DestroyImmediate(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        lastMousePos = Input.mousePosition;
        _videoPlayer.loopPointReached += OnVideoEnded;
        videoRawImage.texture = _videoPlayer.texture;
    }

    public void StartVideoSession(string videoName)
    {
        videoCanvasGO.SetActive(true);
        videoCanvasGO.GetComponent<Canvas>().worldCamera = Camera.main;
        sessionActive = true;
        StartCoroutine(StartVideoSessionTask(videoName));
    }

    IEnumerator StartVideoSessionTask(string videoName)
    {
        _videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, videoName);
        _videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f);
        while (!_videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
        }
        PlayVideo();
    }

    // Update is called once per frame
    void Update()
    {   
        //videoRawImage.rectTransform.sizeDelta = new Vector2((16f/9f)*Screen.height * 0.78f, Screen.height * 0.78f );
        if(sessionActive)
        {
            if(_videoPlayer.isPlaying)
            {
                UpdateTimeText();
            }

            if(_videoPlayer.isPrepared)
            {
                UpdateGauge();
            }  
            CheckForBarVisibility();
        }
    }


	void OnVideoEnded(VideoPlayer vp)
	{
		PauseVideo();
	}

    void CheckForBarVisibility()
    {
        if(!barshown && Vector3.Distance(lastMousePos, Input.mousePosition) > 0.1f)
        {
            ShowBar();
            idleMouseTimer = 0f;
        }
        else if(barshown)
        {
            idleMouseTimer += Time.deltaTime;
            if(idleMouseTimer > 3f)
            {
                idleMouseTimer = 0f;
                if(!Utility.getUIElementAt(videoGR, Input.mousePosition, "videoBar"))
                {
                    HideBar();
                }
                //HideBar();
            }
        }
        lastMousePos = Input.mousePosition;
    }

    void ShowBar()
    {
        if(barshown)
        {
            return;
        }
        barshown = true;
        barRect.DOKill();
        barRect.DOAnchorPosY(11.6f, 0.25f);
    }

    void HideBar()
    {
        if(!barshown)
        {
            return;
        }
        barshown = false;
        barRect.DOKill();
        barRect.DOAnchorPosY(-18.6f, 0.75f);

        volumeBarShown = false;
        volumeSliderRect.gameObject.SetActive(false);
    }

    void UpdateGauge()
    {
        gaugeBar.fillAmount = (float)_videoPlayer.frame / (float)_videoPlayer.frameCount;
        bigMaxPointer.rectTransform.anchoredPosition = Vector2.Lerp(bigMaxPointer.rectTransform.anchoredPosition, new Vector2(gaugeBar.fillAmount * barBack.rectTransform.rect.width, bigMaxPointer.rectTransform.anchoredPosition.y), 0.5f);
    }

    void UpdateTimeText()
    {
        float timeInSecondsLeft = ((float)_videoPlayer.frameCount - (float)_videoPlayer.frame) / _videoPlayer.frameRate;
        timeText.text = timeInSecondsLeft.ToString("0:00");
    }
    
    void PlayVideo()
    {
        pauseButton.enabled = true;
        playButton.enabled = false;
        _videoPlayer.Play();
        videoRawImage.texture = _videoPlayer.texture;
    }

    void PauseVideo()
    {
        pauseButton.enabled = false;
        playButton.enabled = true;
        _videoPlayer.Pause();
    }

    public void OnCloseButtonClick()
    {
        CloseSession();
    }

    void CloseSession()
    {
        //_videoPlayer.frame = 0;
        PauseVideo();
        videoCanvasGO.SetActive(false);
        sessionActive = false;
    }

    public void OnVolumeButtonClick()
    {
        if(!volumeBarShown)
        {
            volumeBarShown = true;
            volumeSliderRect.gameObject.SetActive(true);
        }
        else
        {
            volumeBarShown = false;
            volumeSliderRect.gameObject.SetActive(false);
        }
    }

    public void OnVolumeSliderChanged()
    {
        _videoPlayer.SetDirectAudioVolume(0, volumeSlider.value);
    }

    public void OnVideoTextureClick()
    {
        if(!_videoPlayer.isPrepared)
        {
            return;
        }
        if(!_videoPlayer.isPlaying)
        {
            PlayVideo();
        }
        else
        {
            PauseVideo();
        }
    }

    public void OnPlayButtonClick()
    {
        if(_videoPlayer.isPrepared && !_videoPlayer.isPlaying)
        {
            PlayVideo();
        }
    }

    public void OnPauseButtonClick()
    {
        if(_videoPlayer.isPrepared && _videoPlayer.isPlaying)
        {
            PauseVideo();
        }
    }

    public void OnReplayButtonClick()
    {
        if(_videoPlayer.isPrepared)
        {
            _videoPlayer.frame = 0;
            UpdateTimeText();
            UpdateGauge();
            PlayVideo();
        }
    }

    public void OnClickedToTimeLine()
    {
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(gaugeBar.rectTransform, Input.mousePosition, Camera.main,out localPoint);
        localPoint.x = Mathf.Clamp(localPoint.x, 0f, barBack.rectTransform.rect.width);
        _videoPlayer.frame = (long)((localPoint.x / barBack.rectTransform.rect.width) * _videoPlayer.frameCount);
    }

}
