﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MapEpisode : MonoBehaviour
{
    public Transform bubbleParent;
    public string sceneName;
    bool shown = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowBubble()
    {
        if(shown)
        {
            return;
        }
        shown = true;
        bubbleParent.DOKill();
        bubbleParent.localScale = Vector3.one * 0.06f;
        bubbleParent.DOScale(0.12f, 1.5f).SetEase(Ease.OutElastic);
    }

    public void HideBubble()
    {
        if(!shown)
        {
            return;
        }
        shown = false;
        bubbleParent.DOKill();
        bubbleParent.DOScale(0f, 1f).SetEase(Ease.OutSine);
    }

    private void OnMouseUpAsButton()
    {
        GameMapManager.Instance.LoadScene(sceneName);
    }
}
