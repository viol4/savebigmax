﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FunFact", menuName = "Kidstellar/FunFact", order = 1)]
public class FunFact : ScriptableObject
{
    public string factName;
    public string factContent;
    public int episode;

}

