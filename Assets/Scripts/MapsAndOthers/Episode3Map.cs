﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Episode3Map : MonoBehaviour
{
    bool passingAnotherScene = false;

    public static Episode3Map Instance;
    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	IEnumerator Start () 
	{
		//Application.targetFrameRate = 60;
		yield return FaderController.Instance.unfadeScreen();
	}

    // Update is called once per frame
    void Update()
    {
        
    }



    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
