﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PingPongSimple : MonoBehaviour
{
    public Vector3[] moveVectors;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveTask());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator MoveTask()
    {
        while(true)
        {
            for (int i = 0; i < moveVectors.Length; i++)
            {
                yield return transform.DOMove(moveVectors[i], 0.5f).SetRelative().WaitForCompletion();
            }
        }
    }
}
