﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FaderController : MonoBehaviour
{
    public GameObject faderCanvasGO;
    public Image faderImage;
    public float fadeTime = 0.25f;

    public static FaderController Instance;
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(faderCanvasGO);
        }
        else
        {
            DestroyImmediate(faderCanvasGO);
            DestroyImmediate(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator fadeScreen()
    {
        faderCanvasGO.SetActive(true);
        faderImage.DOFade(1f, fadeTime);
        yield return new WaitForSeconds(fadeTime);
    }

    public IEnumerator unfadeScreen(float delay = 0f)
    {
        faderImage.DOFade(0f, fadeTime).SetDelay(delay);
        yield return new WaitForSeconds(fadeTime + delay);
        faderCanvasGO.SetActive(false);
    }
}
