﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraResolutionFix : MonoBehaviour 
{
	float lastWindowAspect;
	void Awake() 
    {
		lastWindowAspect = (float)Screen.width / (float)Screen.height;
    }
 

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Fix();
	}

	void Fix()
	{
		// set the desired aspect ratio (the values in this example are
        // hard-coded for 16:9, but you could make them into public
        // variables instead so you can set them at design time)
        float targetaspect = 16.0f / 6.5625f;
 
        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;
		if(lastWindowAspect.Equals(windowaspect))
		{
			return;
		}

        // current viewport height should be scaled by this amount
        float scaleheight = windowaspect / targetaspect;
 
        // obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();
 
        // if scaled height is less than current height, add letterbox
        if (scaleheight > 1.0f) 
		{
            float scalewidth = 1.0f / scaleheight;
 
            Rect rect = camera.rect;
 
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
 
            camera.rect = rect;
        }
		lastWindowAspect = windowaspect;

	}
}
