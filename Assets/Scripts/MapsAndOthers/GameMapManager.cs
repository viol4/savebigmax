﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMapManager : MonoBehaviour
{
    public Camera camera;
    public MapEpisode[] mapEpisodes;
    bool passingAnotherScene = false;
    bool zoomedOut = false;

    public static GameMapManager Instance;
    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	IEnumerator Start () 
	{
		//Application.targetFrameRate = 60;
        camera.orthographicSize = 5f * DeviceLifeSaver.Instance.scaleValue;
		yield return FaderController.Instance.unfadeScreen();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

    void ShowBubbles()
    {
        for (int i = 0; i < mapEpisodes.Length; i++)
        {
            mapEpisodes[i].ShowBubble();
        }
    }

    void HideBubbles()
    {
        for (int i = 0; i < mapEpisodes.Length; i++)
        {
            mapEpisodes[i].HideBubble();
        }
    }

    public void OnClickPlanet(string factName)
    {
        FunFactManager.Instance.ShowFact(factName);
    }

    public void OnClickZoomButton()
    {
        if(!zoomedOut)
        {
            zoomedOut = true;
            camera.DOKill();
            camera.DOOrthoSize(25f * DeviceLifeSaver.Instance.scaleValue, 8f).SetSpeedBased().SetEase(Ease.InOutSine);
            HideBubbles();
        }
        else
        {
            zoomedOut = false;
            camera.DOKill();
            camera.DOOrthoSize(5f * DeviceLifeSaver.Instance.scaleValue, 8f).SetSpeedBased().SetEase(Ease.InOutSine).OnComplete(() => 
            {
                ShowBubbles();
            });
            
        }
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
