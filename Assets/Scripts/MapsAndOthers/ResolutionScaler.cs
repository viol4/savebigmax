﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionScaler : MonoBehaviour
{
    public RectTransform targetRect;
    float lastWindowAspect;
	void Awake() 
    {
		lastWindowAspect = (float)Screen.width / (float)Screen.height;
    }
 

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Fix();
	}

	void Fix()
	{
        float targetaspect = 16.0f / 9f;
        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;
		if(lastWindowAspect.Equals(windowaspect))
		{
			return;
		}
        targetRect.sizeDelta = new Vector2(targetaspect * 0.87f *Screen.height, Screen.height * 0.87f);
        lastWindowAspect = windowaspect;
        

	}
}
