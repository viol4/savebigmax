﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Episode4Map : MonoBehaviour
{
    bool passingAnotherScene = false;

    public static Episode4Map Instance;
    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	IEnumerator Start () 
	{
		//Application.targetFrameRate = 60;
		yield return FaderController.Instance.unfadeScreen();
	}

    // Update is called once per frame
    void Update()
    {
        
    }



    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		if(sceneName.Equals("WayBackHome"))
		{
			WayBackHomeManager.restarted = false;
		}
		SceneManager.LoadScene(sceneName);
	}
}
