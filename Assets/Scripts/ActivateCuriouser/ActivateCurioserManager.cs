﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ActivateCurioserManager : MonoBehaviour
{
    public int point = 0;
    public int star = 0;
    public Text pointText;
    public Text starText;

    public GameObject resultPanelGO;
    public GameObject scorePanelGO;
    public Image fader;
    public Button backButton;

    bool passingAnotherScene;

    public static ActivateCurioserManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return FaderController.Instance.unfadeScreen();
        CurryQuestionGenerator.Instance.PassNextQuestion();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void GainPoint(int amount)
    {
        point += amount;
        pointText.text = point.ToString();
    }

    public void ResetPoints()
    {
        int stars = 0;
        if(point >= 20)
        {
            stars = 3 * (point / 20);
        }
        else if(point >= 10)
        {
            stars = 2;
        }
        else if(point >= 5)
        {
            stars = 1;
        }
        star += stars;
        point = 0;
        CurryQuestionGenerator.Instance.OnWrongAnswer();
        pointText.text = point.ToString();
        starText.text = star.ToString();
    }

    public void OnBackButtonClick()
    {
        CurryQuestionGenerator.Instance.StopGame();
        ResetPoints();
        fader.DOKill();
        fader.DOFade(0.5f, 0.25f);
        backButton.gameObject.SetActive(false);
        scorePanelGO.SetActive(false);
        resultPanelGO.SetActive(true);
        //LoadScene("Episode1Map");
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}

    
}

