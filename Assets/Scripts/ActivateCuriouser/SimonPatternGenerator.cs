﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonPatternGenerator : MonoBehaviour
{
    public Sprite[] iconSprites;

    public static SimonPatternGenerator Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if(Input.GetKeyDown(KeyCode.Space))
        // {
        //     int[] array = GetRandomPattern(5, 5);
        //     string ali = "";
        //     for (int i = 0; i < array.Length; i++)
        //     {
        //         ali += array[i] + " ";
        //     }
        //     Debug.Log(ali);
        // }
    }

    public int[] GetRandomPattern(int variableCount, int length)
    {
        List<int> vars = new List<int>();
        for (int i = 0; i < variableCount; i++)
        {
            vars.Add(i);
        }
        List<int> result = new List<int>();
        for (int i = 0; i < length; i++)
        {
            result.Add(vars[Random.Range(0, vars.Count)]);
        }
        return result.ToArray();
    }
}
