﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PatternUIManager : MonoBehaviour
{
    public RectTransform panel;
    public RectTransform expressionImagePanel;
    public RectTransform patternAnswersPanel;

    public GameObject expressionPrefab;
    public GameObject answerOptionPrefab;

    public string code = "ABCD";
    public Sprite[] sprites;

    int currentSpriteIndex = 0;

    public static PatternUIManager Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        panel.gameObject.SetActive(false);
        ResetUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ResetUI()
    {
        int count = expressionImagePanel.childCount;
        for (int i = 0; i < count; i++)
        {
            DestroyImmediate(expressionImagePanel.GetChild(0).gameObject);
        }

        count = patternAnswersPanel.childCount;
        for (int i = 0; i < count; i++)
        {
            DestroyImmediate(patternAnswersPanel.GetChild(0).gameObject);
        }
    }

    void PopupQuestion()
    {
        CurryPatternQuestion cpq = CurryQuestionGenerator.Instance.GetCurrentPatternQuestion();
        for (int i = 0; i < cpq.patternSequence.Length; i++)
        {
            Instantiate(expressionPrefab, expressionImagePanel).GetComponent<PatternExpImage>().SetImage(sprites[currentSpriteIndex + code.IndexOf(cpq.patternSequence[i])]);
        }

        for (int i = 0; i < expressionImagePanel.childCount; i++)
        {
            if(i != cpq.wantedIndex)
            {
                expressionImagePanel.GetChild(i).GetComponent<PatternExpImage>().Popup();
            }
            else
            {
                expressionImagePanel.GetChild(i).GetComponent<PatternExpImage>().PopupQuestionMark();
            }
            
        }
    }

    void PopDownQuestion()
    {
        CurryPatternQuestion cpq = CurryQuestionGenerator.Instance.GetCurrentPatternQuestion();
        for (int i = 0; i < expressionImagePanel.childCount; i++)
        {
            expressionImagePanel.GetChild(i).GetComponent<PatternExpImage>().Popdown();
        }
    }

    void PrepareAnswerOptions(CurryPatternQuestion cpq)
    {
        List<string> options = new List<string>();
        for (int i = 0; i < cpq.patternSequence.Length; i++)
        {
            if(!options.Contains(cpq.patternSequence[i].ToString()))
            {
                options.Add(cpq.patternSequence[i].ToString());
            }
        }
         
        Utility.ShuffleList(ref options);
        for (int i = 0; i < options.Count; i++)
        {
            Instantiate(answerOptionPrefab, patternAnswersPanel).GetComponent<PatternAnswerOption>().SetOption(sprites[currentSpriteIndex + code.IndexOf(options[i])], options[i]);
        }
    }

    public IEnumerator PreparePatternQuestion(CurryPatternQuestion question)
    {
        currentSpriteIndex = Random.Range(0, 5);
        panel.gameObject.SetActive(true);
        PopupQuestion();
        PrepareAnswerOptions(question);
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().Popdown();
        }
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().Popup();
            yield return new WaitForSeconds(0.3f);
        }

        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().SetInteractable(true);
        }
        CurioserTimeManager.Instance.StartTimer();
    }

    public void CheckForAnswer(PatternAnswerOption option)
    {
        CurioserTimeManager.Instance.StopTimer();
        CurryPatternQuestion cpq = CurryQuestionGenerator.Instance.GetCurrentPatternQuestion();
        if(option.answer.Equals(cpq.patternSequence[cpq.wantedIndex].ToString()))
        {
            option.LightCorrect();
            ActivateCurioserManager.Instance.GainPoint(1);
        }
        else
        {
            ActivateCurioserManager.Instance.ResetPoints();
            option.LightWrong();
            for (int i = 0; i < patternAnswersPanel.childCount; i++)
            {
                if(patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().answer.Equals(cpq.patternSequence[cpq.wantedIndex].ToString()))
                {
                    StartCoroutine(patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().LightCorrectWithAnimation());
                    break;
                }
            }
        }
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().SetInteractable(false);
        }
        StartCoroutine(FinishQuestionTask());
    }

    IEnumerator FinishQuestionTask()
    {
        CurryPatternQuestion cpq = CurryQuestionGenerator.Instance.GetCurrentPatternQuestion();
        expressionImagePanel.GetChild(cpq.wantedIndex).GetComponent<PatternExpImage>().Popup();
        yield return new WaitForSeconds(1f);
        PopDownQuestion();
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().Popdown();
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();
        CurryQuestionGenerator.Instance.PassNextQuestion();
        //sequenceTexts[csq.wantedIndex].color = Color.white;
    }

    public IEnumerator FinishQuestionWithoutAnswerTask()
    {
        CurryPatternQuestion cpq = CurryQuestionGenerator.Instance.GetCurrentPatternQuestion();

        ActivateCurioserManager.Instance.ResetPoints();
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            if (patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().answer.Equals(cpq.patternSequence[cpq.wantedIndex].ToString()))
            {
                StartCoroutine(patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().LightCorrectWithAnimation());
                break;
            }
        }
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().SetInteractable(false);
        }

        expressionImagePanel.GetChild(cpq.wantedIndex).GetComponent<PatternExpImage>().Popup();
        yield return new WaitForSeconds(1f);
        PopDownQuestion();
        for (int i = 0; i < patternAnswersPanel.childCount; i++)
        {
            patternAnswersPanel.GetChild(i).GetComponent<PatternAnswerOption>().Popdown();
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();
        CurryQuestionGenerator.Instance.PassNextQuestion();
        //sequenceTexts[csq.wantedIndex].color = Color.white;
    }
}
