﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PatternExpImage : MonoBehaviour
{
    public Image image;
    public Text questionMark;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetImage(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void Popup()
    {
        questionMark.gameObject.SetActive(false);
        image.gameObject.SetActive(true);
        image.transform.DOKill();
        image.transform.localScale /= 2f;
        image.transform.DOScale(image.transform.localScale * 2f, 1.5f).SetEase(Ease.OutElastic);
    }

    public void PopupQuestionMark()
    {
        image.gameObject.SetActive(false);
        questionMark.gameObject.SetActive(true);
        questionMark.transform.DOKill();
        questionMark.transform.localScale /= 2f;
        questionMark.transform.DOScale(questionMark.transform.localScale * 2f, 1.5f).SetEase(Ease.OutElastic).OnComplete(() => 
        {
            questionMark.transform.DOScale(questionMark.transform.localScale * 1.3f, 1f).SetLoops(-1, LoopType.Yoyo);
        });
    }

    public void Popdown()
    {
        image.transform.DOKill();
        image.transform.DOScale(0f, 0.5f).SetEase(Ease.InBack);
    }
    
    public void HideImage()
    {
        image.enabled = false;
    }

    
}
