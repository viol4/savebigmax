﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MathUIManager : MonoBehaviour
{
    public RectTransform panel;
    public MathAnswerOption[] mathAnswerOptions;
    public Text firstNumberText;
    public Text operationText;
    public Text secondNumberText;
    public Text equalsText;
    public Text questionMarkText;

    public static MathUIManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        panel.gameObject.SetActive(false);
        ResetUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ResetUI()
    {
        firstNumberText.transform.DOKill();
        operationText.transform.DOKill();
        secondNumberText.transform.DOKill();
        equalsText.transform.DOKill();
        questionMarkText.transform.DOKill();

        firstNumberText.transform.localScale = Vector3.one * 0.5f;
        operationText.transform.localScale = Vector3.one * 0.5f;
        secondNumberText.transform.localScale  = Vector3.one * 0.5f;
        equalsText.transform.localScale  = Vector3.one * 0.5f;
        questionMarkText.transform.localScale  = Vector3.one * 0.5f;
    }

    void PopupQuestion()
    {
        firstNumberText.transform.DOKill();
        operationText.transform.DOKill();
        secondNumberText.transform.DOKill();
        equalsText.transform.DOKill();
        questionMarkText.transform.DOKill();

        firstNumberText.transform.DOScale(firstNumberText.transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
        operationText.transform.DOScale(operationText.transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
        secondNumberText.transform.DOScale(secondNumberText.transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
        equalsText.transform.DOScale(equalsText.transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
        questionMarkText.transform.DOScale(questionMarkText.transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
    }

    void PopDownQuestion()
    {
        firstNumberText.transform.DOKill();
        operationText.transform.DOKill();
        secondNumberText.transform.DOKill();
        equalsText.transform.DOKill();
        questionMarkText.transform.DOKill();


        firstNumberText.transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
        operationText.transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
        secondNumberText.transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
        equalsText.transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
        questionMarkText.transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
    }

    void PrepareAnswerOptions(int correctAnswer)
    {
        List<int> options = new List<int>();
        options.Add(correctAnswer);
        for (int i = 0; i < 2; i++)
        {
            int otherOption = Random.Range(0, 11);
            while(options.Contains(otherOption))
            {
                otherOption = Random.Range(0, 11);
            }
            options.Add(otherOption);
        }
        Utility.ShuffleList(ref options);
        for (int i = 0; i < options.Count; i++)
        {
            mathAnswerOptions[i].SetOption(options[i]);
        }
    }

    public IEnumerator PrepareMathQuestion(CurryMathQuestion question)
    {
        firstNumberText.text = question.firstNumber.ToString();
        operationText.text = question.operation == 0 ? "-" : "+";
        secondNumberText.text = question.secondNumber.ToString();
        questionMarkText.text = "?";
        PrepareAnswerOptions(question.result);
        panel.gameObject.SetActive(true);
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].gameObject.SetActive(false);
        }
        PopupQuestion();
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);
        }

        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].SetInteractable(true);
        }
        CurioserTimeManager.Instance.StartTimer();
    }

    public void CheckForAnswer(MathAnswerOption option)
    {
        CurioserTimeManager.Instance.StopTimer();
        if(int.Parse(option.optionText.text) == CurryQuestionGenerator.Instance.GetCurrentMathQuestion().result)
        {
            option.LightCorrect();
            ActivateCurioserManager.Instance.GainPoint(1);
        }
        else
        {
            ActivateCurioserManager.Instance.ResetPoints();
            option.LightWrong();
            for (int i = 0; i < mathAnswerOptions.Length; i++)
            {
                if(int.Parse(mathAnswerOptions[i].optionText.text) == CurryQuestionGenerator.Instance.GetCurrentMathQuestion().result)
                {
                    StartCoroutine(mathAnswerOptions[i].LightCorrectWithAnimation());
                    break;
                }
            }
        }
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].SetInteractable(false);
        }
        StartCoroutine(FinishQuestionTask());
    }

    IEnumerator FinishQuestionTask()
    {
        questionMarkText.transform.DOKill();
        questionMarkText.transform.localScale /= 2f;
        questionMarkText.color = mathAnswerOptions[0].greenColor;
        questionMarkText.text = CurryQuestionGenerator.Instance.GetCurrentMathQuestion().result.ToString();
        yield return questionMarkText.transform.DOScale(questionMarkText.transform.localScale * 2f, 1f).SetEase(Ease.OutElastic).WaitForCompletion();
        yield return new WaitForSeconds(0.3f);
        PopDownQuestion();
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].Reset();
            mathAnswerOptions[i].gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();

        questionMarkText.color = Color.white;
        questionMarkText.text = "?";

        CurryQuestionGenerator.Instance.PassNextQuestion();
    }

    public IEnumerator FinishQuestionWithoutAnswerTask()
    {

        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].SetInteractable(false);
        }

        ActivateCurioserManager.Instance.ResetPoints();
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            if (int.Parse(mathAnswerOptions[i].optionText.text) == CurryQuestionGenerator.Instance.GetCurrentMathQuestion().result)
            {
                StartCoroutine(mathAnswerOptions[i].LightCorrectWithAnimation());
                break;
            }
        }
        

        questionMarkText.transform.DOKill();
        questionMarkText.transform.localScale /= 2f;
        questionMarkText.color = mathAnswerOptions[0].greenColor;
        questionMarkText.text = CurryQuestionGenerator.Instance.GetCurrentMathQuestion().result.ToString();
        yield return questionMarkText.transform.DOScale(questionMarkText.transform.localScale * 2f, 1f).SetEase(Ease.OutElastic).WaitForCompletion();
        yield return new WaitForSeconds(0.3f);
        PopDownQuestion();
        for (int i = 0; i < mathAnswerOptions.Length; i++)
        {
            mathAnswerOptions[i].Reset();
            mathAnswerOptions[i].gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();

        questionMarkText.color = Color.white;
        questionMarkText.text = "?";

        CurryQuestionGenerator.Instance.PassNextQuestion();
    }

    
}
