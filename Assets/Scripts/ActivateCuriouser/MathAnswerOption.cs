﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MathAnswerOption : MonoBehaviour
{
    public Image optionBack;
    public Text optionText;

    public Color redColor;
    public Color greenColor;
    bool interactable = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        if(!interactable)
        {
            return;
        }
        MathUIManager.Instance.CheckForAnswer(this);
    }

    public void LightWrong()
    {
        optionBack.color = redColor;
    }

    public void LightCorrect()
    {
        optionBack.color = greenColor;
    }

    public IEnumerator LightCorrectWithAnimation()
    {
        Color firstColor = optionBack.color;
        for (int i = 0; i < 3; i++)
        {
            optionBack.color = greenColor;
            yield return new WaitForSeconds(0.15f);
            optionBack.color = firstColor;
            yield return new WaitForSeconds(0.15f);
        }
        optionBack.color = greenColor;
    }

    public void Reset()
    {
        optionBack.color = Color.white;
    }

    public void SetInteractable(bool value)
    {
        interactable = value;
    }

    public void SetOption(int answer)
    {
        optionText.text = answer.ToString();
    }
}
