﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SequenceUIManager : MonoBehaviour
{
    public RectTransform panel;
    public SequenceAnswerOption[] seqAnswerOptions;
    public Text[] sequenceTexts;

    public static SequenceUIManager Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        panel.gameObject.SetActive(false);
        ResetUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ResetUI()
    {
        for (int i = 0; i < sequenceTexts.Length; i++)
        {
            sequenceTexts[i].transform.DOKill();
            sequenceTexts[i].transform.localScale = Vector3.one * 0.5f;
        }
    }
    
    void PopupQuestion()
    {
        for (int i = 0; i < sequenceTexts.Length; i++)
        {
            sequenceTexts[i].transform.DOKill();
            sequenceTexts[i].transform.DOScale(sequenceTexts[i].transform.localScale * 2f, Random.Range(1f, 1.6f)).SetEase(Ease.OutElastic);
        }
    }

    void PopDownQuestion()
    {
        for (int i = 0; i < sequenceTexts.Length; i++)
        {
            sequenceTexts[i].transform.DOKill();
            sequenceTexts[i].transform.DOScale(0f, Random.Range(0.5f, 0.8f)).SetEase(Ease.InBack);
        }
    }

    void PrepareAnswerOptions(CurrySequenceQuestion csq)
    {
        List<string> options = new List<string>();
        options.Add(csq.sequence[csq.wantedIndex]);
        if(!csq.isLetter)
        {
            for (int i = 0; i < 2; i++)
            {
                int otherOption = Random.Range(0, 11);
                while (options.Contains(otherOption.ToString()))
                {
                    otherOption = Random.Range(0, 11);
                }
                options.Add(otherOption.ToString());
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                string otherOption = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[Random.Range(0, 26)].ToString();
                while (options.Contains(otherOption.ToString()))
                {
                    otherOption = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[Random.Range(0, 26)].ToString();
                }
                options.Add(otherOption.ToString());
            }
        }
        
        Utility.ShuffleList(ref options);
        for (int i = 0; i < options.Count; i++)
        {
            seqAnswerOptions[i].SetOption(options[i]);
        }
    }

    public IEnumerator PrepareSequenceQuestion(CurrySequenceQuestion question)
    {
        for (int i = 0; i < sequenceTexts.Length; i++)
        {
            sequenceTexts[i].text = question.sequence[i];
        }
        sequenceTexts[question.wantedIndex].text = "?";
        PrepareAnswerOptions(question);
        panel.gameObject.SetActive(true);
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].gameObject.SetActive(false);
        }
        PopupQuestion();
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);
        }

        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].SetInteractable(true);
        }
        CurioserTimeManager.Instance.StartTimer();
    }

    public void CheckForAnswer(SequenceAnswerOption option)
    {
        CurioserTimeManager.Instance.StopTimer();
        CurrySequenceQuestion csq = CurryQuestionGenerator.Instance.GetCurrentSequenceQuestion();
        if(option.optionText.text.Equals(csq.sequence[csq.wantedIndex]))
        {
            option.LightCorrect();
            ActivateCurioserManager.Instance.GainPoint(1);
        }
        else
        {
            option.LightWrong();
            ActivateCurioserManager.Instance.ResetPoints();
            for (int i = 0; i < seqAnswerOptions.Length; i++)
            {
                if(seqAnswerOptions[i].optionText.text.Equals(csq.sequence[csq.wantedIndex]))
                {
                    StartCoroutine(seqAnswerOptions[i].LightCorrectWithAnimation());
                    break;
                }
            }
        }
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].SetInteractable(false);
        }
        StartCoroutine(FinishQuestionTask());
    }

    IEnumerator FinishQuestionTask()
    {
        CurrySequenceQuestion csq = CurryQuestionGenerator.Instance.GetCurrentSequenceQuestion();
        sequenceTexts[csq.wantedIndex].transform.DOKill();
        sequenceTexts[csq.wantedIndex].transform.localScale /= 2f;
        sequenceTexts[csq.wantedIndex].color = seqAnswerOptions[0].greenColor;
        sequenceTexts[csq.wantedIndex].text = csq.sequence[csq.wantedIndex];
        yield return sequenceTexts[csq.wantedIndex].transform.DOScale(sequenceTexts[csq.wantedIndex].transform.localScale * 2f, 1f).SetEase(Ease.OutElastic).WaitForCompletion();
        yield return new WaitForSeconds(0.3f);
        PopDownQuestion();
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].Reset();
            seqAnswerOptions[i].gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();
        sequenceTexts[csq.wantedIndex].color = Color.white;
        
        CurryQuestionGenerator.Instance.PassNextQuestion();
    }

    public IEnumerator FinishQuestionWithoutAnswerTask()
    {
        CurrySequenceQuestion csq = CurryQuestionGenerator.Instance.GetCurrentSequenceQuestion();
        ActivateCurioserManager.Instance.ResetPoints();
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            if (seqAnswerOptions[i].optionText.text.Equals(csq.sequence[csq.wantedIndex]))
            {
                StartCoroutine(seqAnswerOptions[i].LightCorrectWithAnimation());
                break;
            }
        }
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].SetInteractable(false);
        }

        sequenceTexts[csq.wantedIndex].transform.DOKill();
        sequenceTexts[csq.wantedIndex].transform.localScale /= 2f;
        sequenceTexts[csq.wantedIndex].color = seqAnswerOptions[0].greenColor;
        sequenceTexts[csq.wantedIndex].text = csq.sequence[csq.wantedIndex];
        yield return sequenceTexts[csq.wantedIndex].transform.DOScale(sequenceTexts[csq.wantedIndex].transform.localScale * 2f, 1f).SetEase(Ease.OutElastic).WaitForCompletion();
        yield return new WaitForSeconds(0.3f);
        PopDownQuestion();
        for (int i = 0; i < seqAnswerOptions.Length; i++)
        {
            seqAnswerOptions[i].Reset();
            seqAnswerOptions[i].gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);
        }
        yield return new WaitForSeconds(0.5f);

        panel.gameObject.SetActive(false);
        ResetUI();
        sequenceTexts[csq.wantedIndex].color = Color.white;
        
        CurryQuestionGenerator.Instance.PassNextQuestion();
    }
}
