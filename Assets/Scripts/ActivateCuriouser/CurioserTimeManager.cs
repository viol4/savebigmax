﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CurioserTimeManager : MonoBehaviour
{
    public float duration = 20f;
    public Image gaugeBar;
    public Gradient timerGradient;

    float timer = 0f;
    bool timerActive = false;

    public static CurioserTimeManager Instance;
    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gaugeBar.color = timerGradient.Evaluate(gaugeBar.fillAmount);
        if(timerActive)
        {
            timer -= Time.deltaTime;
            gaugeBar.fillAmount = Mathf.Lerp(gaugeBar.fillAmount, (timer / duration), 15f * Time.deltaTime);
            if(timer <= 0f)
            {
                OnRunOutOfTime();
            }
        }
    }

    void Activate()
    {
        timerActive = true;
    }

    void DeActivate()
    {
        timerActive = false;
    }

    void Reset()
    {
        timer = duration;
    }

    void OnRunOutOfTime()
    {
        StopTimer();
        CurryQuestionGenerator.Instance.TimeIsUp();
    }

    public void StopTimer()
    {
        DeActivate();
    }

    public void StartTimer()
    {
        Reset();
        Activate();
    }

    public void Fill()
    {
        gaugeBar.DOKill();
        gaugeBar.DOFillAmount(1f, 0.5f).SetEase(Ease.InOutSine);
    }

    

    
}
