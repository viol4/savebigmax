﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurryQuestionGenerator : MonoBehaviour
{
    public GameObject questionPanel;

    public string[] type1Patterns;
    public string[] type2Patterns;

    CurryQuestion lastQuestion;
    CurryQuestion currentQuestion;

    QuestionType lastQuestionType = QuestionType.None;
    bool doubleQuestionTyped = false;
    int phase = 1;
    bool stopped = false;

    public static CurryQuestionGenerator Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        
    }

    // Update is called once per frame
    void Update()
    {
        // if(Input.GetKeyDown(KeyCode.Space))
        // {
        //     currentQuestion = GenerateSequenceQuestion();
        //     StartCoroutine(SequenceUIManager.Instance.PrepareSequenceQuestion((CurrySequenceQuestion)currentQuestion));
        // }
        // else if(Input.GetKeyDown(KeyCode.U))
        // {
        //     currentQuestion = GenerateMathQuestion();
        //     StartCoroutine(MathUIManager.Instance.PrepareMathQuestion((CurryMathQuestion)currentQuestion));
        // }
        // else if(Input.GetKeyDown(KeyCode.K))
        // {
        //     currentQuestion = GeneratePatternQuestion(1, true);
        //     StartCoroutine(PatternUIManager.Instance.PreparePatternQuestion((CurryPatternQuestion)currentQuestion));
        // }
    }

    void PrepareQuestion(int difficulty)
    {
        List<QuestionType> typeList = new List<QuestionType>();
        typeList.Add(QuestionType.Math);
        typeList.Add(QuestionType.Sequence);
        typeList.Add(QuestionType.Pattern);
        if(doubleQuestionTyped)
        {
            typeList.Remove(lastQuestionType);
            doubleQuestionTyped = false;
            lastQuestionType = QuestionType.None;
        }
        currentQuestion = GenerateQuestion(typeList[Random.Range(0, typeList.Count)], difficulty);
        while(lastQuestion != null && currentQuestion.IsSameQuestionWith(lastQuestion))
        {
            currentQuestion = GenerateQuestion(typeList[Random.Range(0, typeList.Count)], difficulty);
        }

        if(doubleQuestionTyped == false && lastQuestionType == currentQuestion.questionType)
        {
            doubleQuestionTyped = true;
        }
        else if(doubleQuestionTyped == false && lastQuestionType == QuestionType.None)
        {
            lastQuestionType = currentQuestion.questionType;
        }
        lastQuestionType = currentQuestion.questionType;
        lastQuestion = currentQuestion;
    }

    void CheckForSameQuestions(List<CurryQuestion> questionList)
    {
        for (int i = 0; i < questionList.Count; i++)
        {
            while(i > 0 && questionList[i].IsSameQuestionWith(questionList[i-1]) )
            {
                if(questionList[i].questionType == QuestionType.Math)
                {
                    questionList[i] = GenerateMathQuestion();
                }
                else if(questionList[i].questionType == QuestionType.Sequence)
                {
                    questionList[i] = GenerateSequenceQuestion();
                }
                else if(questionList[i].questionType == QuestionType.Pattern)
                {
                    questionList[i] = GeneratePatternQuestion(((CurryPatternQuestion)questionList[i]).patternType, ((CurryPatternQuestion)questionList[i]).lastInstance);
                }
            }
        }
    }

    public void StopGame()
    {
        stopped = true;
        StopAllCoroutines();
        questionPanel.SetActive(false);
    }

    public void TimeIsUp()
    {
        if(stopped)
        {
            return;
        }
        if(currentQuestion.questionType == QuestionType.Math)
        {
            StartCoroutine(MathUIManager.Instance.FinishQuestionWithoutAnswerTask());
        }
        else if(currentQuestion.questionType == QuestionType.Sequence)
        {
            StartCoroutine(SequenceUIManager.Instance.FinishQuestionWithoutAnswerTask());
        }
        else if(currentQuestion.questionType == QuestionType.Pattern)
        {
            StartCoroutine(PatternUIManager.Instance.FinishQuestionWithoutAnswerTask());
        }
    }

    public void PassNextQuestion()
    {
        CurioserTimeManager.Instance.Fill();
        if(phase <= 5)
        {
            PrepareQuestion(0);
        }
        else if(phase <= 15)
        {
            PrepareQuestion(1);
        }
        else
        {
            PrepareQuestion(2);
        }
        if(currentQuestion.questionType == QuestionType.Sequence)
        {
            StartCoroutine(SequenceUIManager.Instance.PrepareSequenceQuestion((CurrySequenceQuestion)currentQuestion));
        }
        else if(currentQuestion.questionType == QuestionType.Math)
        {
            StartCoroutine(MathUIManager.Instance.PrepareMathQuestion((CurryMathQuestion)currentQuestion));
        }
        else if(currentQuestion.questionType == QuestionType.Pattern)
        {
            StartCoroutine(PatternUIManager.Instance.PreparePatternQuestion((CurryPatternQuestion)currentQuestion));
        }
        phase++;
    }

    public void OnWrongAnswer()
    {
        phase--;
    }

    public CurryQuestion GetCurrentQuestion()
    {
        return currentQuestion;
    }

    public CurryMathQuestion GetCurrentMathQuestion()
    {
        return (CurryMathQuestion)currentQuestion;
    }

    public CurrySequenceQuestion GetCurrentSequenceQuestion()
    {
        return (CurrySequenceQuestion)currentQuestion;
    }

    public CurryPatternQuestion GetCurrentPatternQuestion()
    {
        return (CurryPatternQuestion)currentQuestion;
    }

    public CurryQuestion GenerateQuestion(QuestionType type, int difficulty)
    {
        if(type == QuestionType.Math)
        {
            return GenerateMathQuestion();
        }
        else if(type == QuestionType.Sequence)
        {
            return GenerateSequenceQuestion();
        }
        else if(type == QuestionType.Pattern)
        {
            if(difficulty == 0)
            {
                return GeneratePatternQuestion(1, true);
            }
            else if(difficulty == 1)
            {
                return GeneratePatternQuestion(1, false);
            }
            else if(difficulty == 2)
            {
                return GeneratePatternQuestion(2, false);
            }
        }
        return null;
    }

    CurryMathQuestion GenerateMathQuestion()
    {
        CurryMathQuestion cmq = new CurryMathQuestion();
        cmq.questionType = QuestionType.Math;
        cmq.operation = Random.Range(0, 2);
        cmq.firstNumber = Random.Range(0, 11);
        cmq.secondNumber = cmq.operation == 0 ? Random.Range(0, cmq.firstNumber) : Random.Range(0, 11);
        cmq.result = cmq.operation == 0 ? cmq.firstNumber - cmq.secondNumber : cmq.firstNumber + cmq.secondNumber;
        return cmq;
    }

    CurryPatternQuestion GeneratePatternQuestion(int type, bool lastInstance)
    {
        CurryPatternQuestion cpq = new CurryPatternQuestion();
        if(type == 1)
        {
            cpq.patternSequence = type1Patterns[Random.Range(0, type1Patterns.Length)];
            if(lastInstance)
            {
                cpq.wantedIndex = cpq.patternSequence.Length - 1;
            }
            else
            {
                cpq.wantedIndex = Random.Range(0, cpq.patternSequence.Length);
            }
        }
        else if(type == 2)
        {
            List<int> numbers = new List<int>();
            for (int i = 0; i < 6; i++)
            {
                numbers.Add(i);
            }
            int index = Random.Range(0, type2Patterns.Length);
            cpq.patternSequence = type2Patterns[index];
            if(index == 0)
            {
                numbers.RemoveAt(2);
                numbers.RemoveAt(3);
            }
            else if(index == 1)
            {
                numbers.RemoveAt(2);
                numbers.RemoveAt(3);
            }
            cpq.wantedIndex = numbers[Random.Range(0, numbers.Count)];
        }
        cpq.lastInstance = lastInstance;
        cpq.patternType = type;
        return cpq;
    }

    CurrySequenceQuestion GenerateSequenceQuestion()
    {
        List<string> characterList = new List<string>();
        int startIndex = 0;
        bool isLetter = false;
        if(Random.value > 0.5f)
        {
            for (int i = 0; i < "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Length; i++)
            {
                characterList.Add("ABCDEFGHIJKLMNOPQRSTUVWXYZ"[i].ToString());
            }
            startIndex = Random.Range(0, "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Length - 3);
            isLetter = true;
        }
        else
        {
            for (int i = 0; i < 11; i++)
            {
                characterList.Add(i.ToString());
            }
            startIndex = Random.Range(0, 8);
        }
        
        string[] sequence = new string[4];
        for (int i = 0; i < 4; i++)
        {
            sequence[i] = characterList[startIndex + i];
        }
        CurrySequenceQuestion csq = new CurrySequenceQuestion();
        csq.sequence = sequence;
        csq.wantedIndex = Random.Range(0, sequence.Length);
        csq.isLetter = isLetter;
        return csq;
    }


}

public class CurryQuestion
{
    public string questionName = "Question #";
    public string questionExpression;
    public QuestionType questionType;

    public CurryQuestion()
    {

    }

    public CurryQuestion(CurryQuestion other)
    {
        questionName = other.questionName;
        questionExpression = other.questionExpression;
        questionType = other.questionType;
    }

    public virtual bool IsSameQuestionWith(CurryQuestion other)
    {
        return other.questionType == questionType;
    }

    public virtual void PrintQuestion()
    {
        Debug.Log(questionType);
    }
}

public class CurryPatternQuestion : CurryQuestion
{
    public string patternSequence;
    public int wantedIndex;
    public bool lastInstance;
    public int patternType;

    public CurryPatternQuestion() : base()
    {
        questionType = QuestionType.Pattern;
    }

    public CurryPatternQuestion(CurryPatternQuestion csq) : base(csq)
    {
        patternSequence = csq.patternSequence;
        wantedIndex = csq.wantedIndex;
        lastInstance = csq.lastInstance;
        patternType = csq.patternType;
    }

    public override bool IsSameQuestionWith(CurryQuestion other)
    {
        if(base.IsSameQuestionWith(other))
        {
            return patternSequence.Equals(((CurryPatternQuestion)other).patternSequence);
        }
        return false;
    }

    public override void PrintQuestion()
    {
        string print = "";
        for (int i = 0; i < patternSequence.Length; i++)
        {
            print += patternSequence[i] + " ";
        }
        print += "====== " + wantedIndex;
        Debug.Log(print);
    }
}

public class CurrySequenceQuestion : CurryQuestion
{
    public string[] sequence;
    public int wantedIndex;
    public bool isLetter;

    public CurrySequenceQuestion() : base()
    {
        questionType = QuestionType.Sequence;
    }

    public CurrySequenceQuestion(CurrySequenceQuestion csq) : base(csq)
    {
        sequence = csq.sequence;
        wantedIndex = csq.wantedIndex;
        isLetter = csq.isLetter;
    }

    public override bool IsSameQuestionWith(CurryQuestion other)
    {
        if(!base.IsSameQuestionWith(other))
        {
            return false;
        }
        for (int i = 0; i < sequence.Length; i++)
        {
            if(sequence[i] != ((CurrySequenceQuestion)other).sequence[i])
            {
                return false;
            }    
        }
        if(wantedIndex != ((CurrySequenceQuestion)other).wantedIndex)
        {
            return false;
        }
        return true;
    }

    public override void PrintQuestion()
    {
        string print = "";
        for (int i = 0; i < sequence.Length; i++)
        {
            print += sequence[i] + " ";
        }
        print += "%%%%% " + wantedIndex;
        Debug.Log(print);
    }
}

public class CurryMathQuestion : CurryQuestion
{
    public int firstNumber;
    public int secondNumber;
    public int operation;//0 for substraction
    public int result;

    public CurryMathQuestion() : base()
    {
        questionType = QuestionType.Math;
    }

    public CurryMathQuestion(CurryMathQuestion cmq) : base(cmq)
    {
        firstNumber = cmq.firstNumber;
        secondNumber = cmq.secondNumber;
        operation = cmq.operation;
        result = cmq.result;
    }

    public override bool IsSameQuestionWith(CurryQuestion other)
    {
        if(!base.IsSameQuestionWith(other))
        {
            return false;
        }
        return 
        (((CurryMathQuestion)other).firstNumber == firstNumber) && 
        (((CurryMathQuestion)other).secondNumber == secondNumber) &&
        (((CurryMathQuestion)other).operation == operation) &&
        (((CurryMathQuestion)other).result == result);
    }

    public override void PrintQuestion()
    {
        Debug.Log(firstNumber + " " + (operation == 0 ? "-" : "+") + " " + secondNumber + " = " + result);
    }
}

public enum QuestionType
{
    None, Math, Sequence, Pattern
}
