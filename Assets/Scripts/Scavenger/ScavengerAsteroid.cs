﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ScavengerAsteroid : MonoBehaviour
{
    public ScavengerAsteroidType asteroidType;
    public GameObject explosionParticleGO;
    public SpriteRenderer sprite;
    public float rotateSpeed = 100f;
    public float moveSpeed = 1f;
    public int crashEnergy = 1;

    public Color[] shifterColors;

    bool destroyed = false;
    float timer = 0;
    float shifterTimer = 0f;
    bool trafficLightShowed = false;
    Vector3 currentPosition;
    // Start is called before the first frame update
    void Start()
    {
        if(asteroidType == ScavengerAsteroidType.Snail)
        {
            StartMoveForSnailAndRabbit();
        }
        else if(asteroidType == ScavengerAsteroidType.Rabbit)
        {
            StartMoveForSnailAndRabbit();
        }
        else if(asteroidType == ScavengerAsteroidType.Shifter)
        {
            StartMoveForShifter();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        sprite.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
        timer += Time.deltaTime;
        if(timer > 5f)
        {
            if(transform.position.x > 13f || transform.position.x < -13f || transform.position.y > 6f || transform.position.y < -6f)
            {
                DestroyAsteroid(false);
            }
        }
        if(asteroidType == ScavengerAsteroidType.Shifter)
        {
            shifterTimer += Time.deltaTime;
            if(shifterTimer >= 15f)
            {
                shifterTimer = 0f;
                trafficLightShowed = false;
                sprite.DOKill();
                sprite.DOColor(Color.white, 1f);
                StartMoveShifterAnotherPlace(3f);
            }
            else if(shifterTimer >= 11f)
            {
                if(!trafficLightShowed)
                {
                    trafficLightShowed = true;
                    StartCoroutine(TrafficLightEffectTask());
                }
                
            }
        }
        
    }

    void StartMoveForSnailAndRabbit()
    {
        rotateSpeed = Random.Range(60f, 150f);
        float angle = Utility.angleBetween(transform.position,RocketHookManager.Instance.GetPositionOfRocket());
        angle += Random.Range(-30f, 30f);
        Vector3 direction = Utility.AngleToDirection(angle);
        transform.DOMove(direction.normalized * 50f, moveSpeed).SetSpeedBased().SetRelative();
    }

    void StartMoveForShifter()
    {
        rotateSpeed = Random.Range(60f, 150f);
        transform.position = AsteroidGenerator.Instance.GetARandomPosShifter();
        currentPosition = transform.position;
        Vector3 firstScale = transform.localScale;
        transform.localScale = firstScale / 2f;
        transform.DOScale(firstScale, 1.5f).SetEase(Ease.OutElastic);
    }

    void StartMoveShifterAnotherPlace(float distance)
    {
        if(distance >= 20f)
        {
            return;
        }
        List<Vector3> positions = AsteroidGenerator.Instance.GetAllPositionListShifter();
        List<Vector3> goodPositions = new List<Vector3>();
        for (int i = 0; i < positions.Count; i++)
        {
            if(!AsteroidGenerator.Instance.IsPosOccupiedByAnotherAsteroid(positions[i]) &&
            Vector3.Distance(transform.position, positions[i]) <= distance)
            {
                goodPositions.Add(positions[i]);
            }
        }
        if(goodPositions.Count == 0)
        {
            StartMoveShifterAnotherPlace(distance + 5f);
        }
        else
        {
            currentPosition = goodPositions[Random.Range(0, goodPositions.Count)];
            transform.DOMove(currentPosition, 5f).SetEase(Ease.InOutSine);
        }
    }

    public Vector3 GetCurrentPosition()
    {
        return currentPosition;
    }

    public void DestroyAsteroid(bool particle = true, bool withRemove = true)
    {
        if(destroyed)
        {
            return;
        }
        destroyed = true;
        if(particle)
        {
            GameObject explosionGO = Instantiate(explosionParticleGO);
            explosionGO.transform.position = transform.position;
            Destroy(explosionGO, 2f);
        }
        if(withRemove)
        {
            AsteroidGenerator.Instance.RemoveAsteroid(this);
        }
        Destroy(gameObject);
    }

    IEnumerator TrafficLightEffectTask()
    {
        for (int i = 0; i < shifterColors.Length; i++)
        {
            sprite.DOKill();
            yield return sprite.DOColor(shifterColors[i], 1f).WaitForCompletion();
        }
    }

}

public enum ScavengerAsteroidType
{
    None,Snail,Rabbit,Shifter
}
