﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour
{
    public Transform spawnPlacesParent;
    public Transform shifterSpawnPlaces;
    public Transform asteroidParent;
    public GameObject asteroidPrefab;
    public GameObject rabbitAsteroidPrefab;
    public GameObject shifterAsteroidPrefab;

    List<ScavengerAsteroid> asteroidList;
    float timer = 0f;

    public static AsteroidGenerator Instance;
    void Awake()
    {
        Instance = this;
        asteroidList = new List<ScavengerAsteroid>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //SpawnAsteroids(3);
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public void DestroyAllAsteroids()
    {
        int count = asteroidList.Count;
        for (int i = 0; i < count; i++)
        {
            asteroidList[i].DestroyAsteroid(true, false);
        }
        asteroidList.Clear();
    }

    public void SpawnAsteroids()
    {
        if(ScavengerManager.Instance.currentStage == 2 || ScavengerManager.Instance.currentStage == 3)
        {
            Vector3[] positions = GetRandomPositions(3);
            for (int i = 0; i < positions.Length; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Snail);
            }
        }
        else if(ScavengerManager.Instance.currentStage == 4)
        {
            Vector3[] positions = GetRandomPositions(4);
            for (int i = 0; i < 2; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Snail);
            }
            for (int i = 2; i < 4; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Rabbit);
            }
        }
        else if(ScavengerManager.Instance.currentStage >= 5)
        {
            Vector3[] positions = GetRandomPositions(6);
            for (int i = 0; i < 2; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Snail);
            }
            for (int i = 2; i < 4; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Rabbit);
            }
            for (int i = 4; i < 6; i++)
            {
                SpawnAsteroid(positions[i], ScavengerAsteroidType.Shifter);
            }
        }
        
    }

    public void SpawnAsteroid(Vector3 pos, ScavengerAsteroidType type)
    {
        if(type == ScavengerAsteroidType.Snail)
        {
            ScavengerAsteroid asteroid = Instantiate(asteroidPrefab, asteroidParent).GetComponent<ScavengerAsteroid>();
            asteroid.transform.position = pos;
            asteroidList.Add(asteroid);
        }
        else if(type == ScavengerAsteroidType.Rabbit)
        {
            ScavengerAsteroid asteroid = Instantiate(rabbitAsteroidPrefab, asteroidParent).GetComponent<ScavengerAsteroid>();
            asteroid.transform.position = pos;
            asteroidList.Add(asteroid);
        }
        else if(type == ScavengerAsteroidType.Shifter)
        {
            ScavengerAsteroid asteroid = Instantiate(shifterAsteroidPrefab, asteroidParent).GetComponent<ScavengerAsteroid>();
            asteroidList.Add(asteroid);
        }
        
    }

    Vector3[] GetRandomPositions(int count)
    {
        Vector3[] poses = new Vector3[count];
        List<Vector3> positionList = new List<Vector3>();
        for (int i = 0; i < spawnPlacesParent.childCount; i++)
        {
            positionList.Add(spawnPlacesParent.GetChild(i).position);
        }
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, positionList.Count);
            poses[i] = positionList[index];
            positionList.RemoveAt(index);
        }
        return poses;
    }

    Vector3 GetARandomPos()
    {
        return spawnPlacesParent.GetChild(Random.Range(0, spawnPlacesParent.childCount)).position;
    }

    public bool IsPosOccupiedByAnotherAsteroid(Vector3 pos)
    {
        for (int i = 0; i < asteroidList.Count; i++)
        {
            if(asteroidList[i].GetCurrentPosition().Equals(pos))
            {
                return true;
            }
        }
        return false;
    }

    public List<Vector3> GetAllPositionListShifter()
    {
        List<Vector3> positionList = new List<Vector3>();
        for (int i = 0; i < shifterSpawnPlaces.childCount; i++)
        {
            positionList.Add(shifterSpawnPlaces.GetChild(i).position);
        }
        return positionList;
    }

    public Vector3 GetARandomPosShifter()
    {
        List<Vector3> positionList = new List<Vector3>();
        for (int i = 0; i < shifterSpawnPlaces.childCount; i++)
        {
            positionList.Add(shifterSpawnPlaces.GetChild(i).position);
        }
        Utility.ShuffleList<Vector3>(ref positionList);
        for (int i = 0; i < positionList.Count; i++)
        {
            if(!IsPosOccupiedByAnotherAsteroid(positionList[i]))
            {
                return positionList[i];
            }
        }
        return positionList[0];
    }

    public void RemoveAsteroid(ScavengerAsteroid asteroid)
    {
        SpawnAsteroid(GetARandomPos(), asteroid.asteroidType);
        asteroidList.Remove(asteroid);
    }

    
}
