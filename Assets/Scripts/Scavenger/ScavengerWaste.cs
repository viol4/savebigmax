﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ScavengerWaste : MonoBehaviour
{
    public SpriteRenderer wasteSprite;
    public ParticleSystem catchParticle;
    public ScavengerWasteType wasteType;
    public float rotateSpeed;

    bool caught = false;
    bool destroyed = false;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        Vector3 firstScale = transform.localScale;
        transform.localScale *= 0f;
        rotateSpeed = Random.Range(30f, 90f);
        yield return new WaitForSeconds(Random.Range(0.3f, 1f));
        transform.localScale = firstScale / 2f;
        transform.DOScale(firstScale, 1.5f).SetEase(Ease.OutElastic);
    }

    // Update is called once per frame
    void Update()
    {
        if(!caught)
        {
            wasteSprite.transform.Rotate(rotateSpeed * Time.deltaTime * Vector3.forward);
        }
    }

    public void MakeCaught()
    {
        if(destroyed)
        {
            return;
        }
        caught = true;
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void DestroyYourself()
    {
        if(caught)
        {
            return;
        }
        destroyed = true;
        transform.DOScale(transform.localScale * 0.5f, 0.7f).SetEase(Ease.InBack).OnComplete(() => 
        {
            Destroy(gameObject);
        });
    }
}

public enum ScavengerWasteType
{
    None, Normal, Wreck
}
