﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ScavengerDrone : MonoBehaviour
{
    public Transform target;
    public float speed;

    bool hookGoing = false;
    bool hookComingBack = false;
    bool takingWaste = false;
    bool crashed = false;
    Vector3 hookTargetPos;
    Vector3 lastPos;

    public static ScavengerDrone Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!hookGoing && !hookComingBack && !takingWaste)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, speed * Time.deltaTime);
        }
    }

    public bool IsBusy()
    {
        return hookGoing || hookComingBack || takingWaste;
    }

    public void SetHookTarget(Vector3 hookTarget)
    {
        if(hookGoing || hookComingBack)
        {
            return;
        }
        hookGoing = true;
        lastPos = transform.position;
        hookTargetPos = hookTarget;
        transform.DOMove(hookTarget, 10f).SetSpeedBased().SetEase(Ease.InOutSine).OnUpdate(()=> 
        {
            if(transform.position.y > 5f || transform.position.y < -5f)
            {
                hookGoing = false;
                TakeBackHook();
            }
        }).OnComplete(() => 
        {
            hookGoing = false;
            TakeBackHook();
        });
    }

    void TakeBackHook()
    {
        if(hookComingBack)
        {
            return;
        }
        hookComingBack = true;
        transform.DOKill();
        transform.DOMove(lastPos, 7f).SetSpeedBased().SetEase(Ease.InOutSine).OnComplete(() => 
        {
            hookComingBack = false;
            if(!takingWaste && !crashed)
            {
                ScavengerManager.Instance.AddEnergy(-1);
            }
            else if(takingWaste)
            {
                ScavengerManager.Instance.AddEnergy(1);
            }
            takingWaste = false;
            crashed = false;
        });
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<ScavengerWaste>() && !takingWaste)
        {
            takingWaste = true;
            hookGoing = false;
            StartCoroutine(CatchWasteTask(other.GetComponent<ScavengerWaste>()));
        }
        else if(other.GetComponent<ScavengerAsteroid>() && (hookGoing || hookComingBack))
        {
            other.GetComponent<ScavengerAsteroid>().DestroyAsteroid();
            hookGoing = false;
            ScavengerManager.Instance.AddEnergy(other.GetComponent<ScavengerAsteroid>().crashEnergy);
            crashed = true;
            TakeBackHook();
        }
    }

    IEnumerator CatchWasteTask(ScavengerWaste waste)
    {
        waste.MakeCaught();
        transform.DOKill();

        yield return transform.DOMove(waste.transform.position + Vector3.up * 1f,0.5f).SetEase(Ease.InOutSine).WaitForCompletion();
        yield return waste.transform.DOShakePosition(0.5f, Vector3.one * 0.05f, 20, 5, false, false).SetEase(Ease.InSine).WaitForCompletion();
        waste.transform.DOMove(transform.position, 0.4f);
        waste.transform.DOScale(waste.transform.localScale / 3f, 0.4f);
        yield return new WaitForSeconds(0.4f);
        WasteGenerator.Instance.RemoveWaste(waste);
        Destroy(waste.gameObject);
        ScavengerManager.Instance.GainMaterial(waste.wasteType == ScavengerWasteType.Normal ? 1 : 2);
        transform.DOScale(transform.localScale * 1.5f, 0.1f).OnComplete(()=> 
        {
            transform.DOScale(transform.localScale / 1.5f, 0.5f);
        });
        yield return new WaitForSeconds(0.6f);
        
        TakeBackHook();
    }


}
