﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WasteGenerator : MonoBehaviour
{
    public Transform spawnPlacesParent;
    public Transform wasteParent;
    public GameObject wastePrefab;
    public GameObject wasteUFOPrefab;

    List<ScavengerWaste> wasteList;
    float timer = 0f;

    public static WasteGenerator Instance;
    void Awake()
    {
        Instance = this;
        wasteList = new List<ScavengerWaste>();
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public void SpawnWastes()
    {
        if(ScavengerManager.Instance.currentStage <= 2)
        {
            Vector3[] positions = GetRandomPositions(3);
            for (int i = 0; i < positions.Length; i++)
            {
                SpawnWaste(positions[i], ScavengerWasteType.Normal);
            }
        }
        else if(ScavengerManager.Instance.currentStage == 3)
        {
            Vector3[] positions = GetRandomPositions(3);
            for (int i = 0; i < 2; i++)
            {
                SpawnWaste(positions[i], ScavengerWasteType.Normal);
            }
            for (int i = 2; i < 3; i++)
            {
                SpawnWaste(positions[i], ScavengerWasteType.Wreck);
            }
        }
        else if(ScavengerManager.Instance.currentStage >= 4)
        {
            Vector3[] positions = GetRandomPositions(6);
            for (int i = 0; i < 4; i++)
            {
                SpawnWaste(positions[i], ScavengerWasteType.Normal);
            }
            for (int i = 4; i < 6; i++)
            {
                SpawnWaste(positions[i], ScavengerWasteType.Wreck);
            }
        }
    }

    void SpawnWaste(Vector3 pos, ScavengerWasteType wasteType)
    {
        if(wasteType == ScavengerWasteType.Normal)
        {
            ScavengerWaste waste = Instantiate(wastePrefab, wasteParent).GetComponent<ScavengerWaste>();
            waste.transform.position = pos;
            wasteList.Add(waste);
        }
        else if(wasteType == ScavengerWasteType.Wreck)
        {
            ScavengerWaste waste = Instantiate(wasteUFOPrefab, wasteParent).GetComponent<ScavengerWaste>();
            waste.transform.position = pos;
            wasteList.Add(waste);
        }
        
    }

    Vector3[] GetRandomPositions(int count)
    {
        Vector3[] poses = new Vector3[count];
        List<Vector3> positionList = new List<Vector3>();
        for (int i = 0; i < spawnPlacesParent.childCount; i++)
        {
            positionList.Add(spawnPlacesParent.GetChild(i).position);
        }
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, positionList.Count);
            poses[i] = positionList[index];
            positionList.RemoveAt(index);
        }
        return poses;
    }

    Vector3 GetARandomPos()
    {
        return spawnPlacesParent.GetChild(Random.Range(0, spawnPlacesParent.childCount)).position;
    }

    public void DestroyAllWastes()
    {
        for (int i = 0; i < wasteList.Count; i++)
        {
            wasteList[i].DestroyYourself();
        }
        wasteList.Clear();
    }

    public void RemoveWaste(ScavengerWaste waste)
    {
        wasteList.Remove(waste);
    }

    public bool IsWasteListEmpty()
    {
        Debug.Log(wasteList.Count);
        return wasteList.Count == 0;
    }
}
