﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceLifeSaver : MonoBehaviour
{
    public float scaleValue = 1;
    public static DeviceLifeSaver Instance;
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            if(SystemInfo.operatingSystemFamily == OperatingSystemFamily.Other)
		    {
                scaleValue = 1f;
		    }
        }
        else
        {
            DestroyImmediate(gameObject);
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
