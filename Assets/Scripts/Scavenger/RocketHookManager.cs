﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RocketHookManager : MonoBehaviour
{
    public Transform hookParent;
    public SpriteRenderer hookSprite;
    public SpriteRenderer rocketSprite;
    public Transform hookTraceParent;
    public ParticleSystem traceParticle;
    public Transform itemPlace;
    [Space]
    public float rotateSpeed = 100f;

    bool hookGoing = false;
    bool hookComingBack = false;
    float ropeLineTimer = 0;
    ScavengerWaste currentWaste;

    public static RocketHookManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) && ScavengerManager.Instance.IsSessionActive() && !ScavengerDrone.Instance.IsBusy())
        {
            StartToHook();
        }


        if(!ScavengerDrone.Instance.IsBusy())
        {
            CheckForRotation();
        }
    }

    Vector3 GetTarget()
    {
        return hookTraceParent.GetChild(hookTraceParent.childCount - 1).position;
    }

    void CheckForRotation()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            hookParent.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
        }
        else if(Input.GetKey(KeyCode.RightArrow))
        {
            hookParent.Rotate(Vector3.back * rotateSpeed * Time.deltaTime);
        }
    }

    void StartToHook()
    {
        // Utility.SetParticleEmission(traceParticle, 100);
        traceParticle.Play();
        ScavengerDrone.Instance.SetHookTarget(GetTarget());
    }


    

    void GainEnergyByWaste()
    {
        rocketSprite.transform.DOScale(rocketSprite.transform.localScale * 1.5f, 0.1f).OnComplete(()=> 
        {
            rocketSprite.transform.DOScale(rocketSprite.transform.localScale / 1.5f, 0.5f);
        });
        Destroy(currentWaste.gameObject);
        //currentWaste = null;
    }

    public Vector3 GetPositionOfRocket()
    {
        return rocketSprite.transform.parent.position;
    }
    
}
