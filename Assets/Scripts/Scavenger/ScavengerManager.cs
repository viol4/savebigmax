﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScavengerManager : MonoBehaviour
{
    public int currentStage = 1;
    public int materialsCollected = 0;
    public int energy = 5;

    [Space]

    public Text materialText;
    public Image energyBarImage;
    public Image fader;
    public GameObject resultPanelGO;
    public GameObject backButtonGO;
    public Text starText;
    
    bool sessionActive;
    bool passingAnotherScene;

    public static ScavengerManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return FaderController.Instance.unfadeScreen();
        yield return PassSession();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsSessionActive()
    {
        return sessionActive;
    }

    public void GainMaterial(int amount)
    {
        materialsCollected += amount;
        materialText.text = materialsCollected.ToString();
        if(currentStage == 1 && materialsCollected >= 3)
        {
            currentStage = 2;
            sessionActive = false;
            AsteroidGenerator.Instance.DestroyAllAsteroids();
            WasteGenerator.Instance.DestroyAllWastes();
            StartCoroutine(PassSession());
        }
        else if(currentStage == 2 && materialsCollected >= 6)
        {
            currentStage = 3;
            sessionActive = false;
            AsteroidGenerator.Instance.DestroyAllAsteroids();
            WasteGenerator.Instance.DestroyAllWastes();
            StartCoroutine(PassSession());
        }
        else if(currentStage == 3 && materialsCollected >= 9)
        {
            currentStage = 4;
            sessionActive = false;
            AsteroidGenerator.Instance.DestroyAllAsteroids();
            WasteGenerator.Instance.DestroyAllWastes();
            StartCoroutine(PassSession());
        }
        else if(currentStage == 4 && materialsCollected >= 15)
        {
            currentStage = 5;
            sessionActive = false;
            AsteroidGenerator.Instance.DestroyAllAsteroids();
            WasteGenerator.Instance.DestroyAllWastes();
            StartCoroutine(PassSession());
        }
        else if(currentStage >= 5 && WasteGenerator.Instance.IsWasteListEmpty())
        {
            currentStage++;
            sessionActive = false;
            AsteroidGenerator.Instance.DestroyAllAsteroids();
            WasteGenerator.Instance.DestroyAllWastes();
            StartCoroutine(PassSession());
        }
    }

    public void AddEnergy(int amount)
    {
        energy += amount;
        energy = Mathf.Clamp(energy, 0, 5);
        energyBarImage.DOKill();
        energyBarImage.DOFillAmount(energy / 5f, 0.3f).SetEase(Ease.OutSine).SetSpeedBased();
        if(energy == 0)
        {
            FinishGame();
        }
    }

    void FinishGame()
    {
        sessionActive = false;
        fader.DOFade(0.5f, 0.6f);
        backButtonGO.SetActive(false);
        resultPanelGO.SetActive(true);
        int star = 0;
        if(materialsCollected >= 20)
        {
            star = 3 * (materialsCollected / 20);
        }
        else if(materialsCollected >= 10)
        {
            star = 2;
        }
        else if(materialsCollected >= 5)
        {
            star = 1;
        }
        starText.text = star.ToString();
    }

    public void OnBackButtonClick()
    {
        FinishGame();
    }

    public void OnRestartButtonClick()
    {
        fader.DOKill();
        fader.DOFade(0f, 0.6f);
        resultPanelGO.SetActive(false);
        backButtonGO.SetActive(true);
        currentStage = 1;
        materialsCollected = 0;
        materialText.text = "0";
        AddEnergy(5);
        sessionActive = false;
        AsteroidGenerator.Instance.DestroyAllAsteroids();
        WasteGenerator.Instance.DestroyAllWastes();
        StartCoroutine(PassSession());
    }

    IEnumerator PassSession()
    {
        yield return new WaitForSeconds(1f);
        sessionActive = true;
        WasteGenerator.Instance.SpawnWastes();
        AsteroidGenerator.Instance.SpawnAsteroids();
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
