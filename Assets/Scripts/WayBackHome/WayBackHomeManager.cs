﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WayBackHomeManager : MonoBehaviour
{
    public CharacterMovement[] characters;
    public GameObject characterSelectionPanel;
    public Image fader;
    public GameObject starPanel;
    public Text starText;
    public Button closePanelButton;

    public GameObject[] starterParkours;
    public GameObject[] randomParkours;
    public Transform parkourParent;
    public Checkpoint startCheckpoint;

    int checkPointTouch = 0;
    int level = 0;
    int deathCounter = 0;
    int totalStars = 0;

    int parkourGenIndex = 0;

    bool passingAnotherScene;
    bool started = false;
    bool paused = false;

    public Checkpoint currentCheckpoint;
    Parkour currentParkour;
    List<GameObject> parkourList;
    GameObject lastParkourPrefab;

    List<Parkour> generatedParkours;

    public static bool restarted = false;
    public static int restartPlayerCount = 1;

    public static WayBackHomeManager Instance;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        currentCheckpoint = startCheckpoint;
        parkourList = new List<GameObject>(randomParkours);
        Utility.ShuffleList(ref parkourList);
        generatedParkours = new List<Parkour>();
        GenerateNextParkours();
        if(restarted)
        {
            restarted = false;
            StartGame(restartPlayerCount);
        }
        yield return FaderController.Instance.unfadeScreen();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenerateNextParkours()
    {
        if(level == 0)
        {
            Checkpoint tempCP = currentCheckpoint;
            for (int i = 0; i < starterParkours.Length; i++)
            {
                Parkour parkour = Instantiate(starterParkours[i], parkourParent).GetComponent<Parkour>();
                parkour.transform.position = tempCP.transform.position;
                generatedParkours.Add(parkour);
                tempCP = parkour.checkpoint;
                lastParkourPrefab = starterParkours[i];
            }

            for (int i = 0; i < starterParkours.Length; i++)
            {
                Parkour parkour = Instantiate(GetRandomParkour(), parkourParent).GetComponent<Parkour>();
                parkour.transform.position = tempCP.transform.position;
                generatedParkours.Add(parkour);
                tempCP = parkour.checkpoint;
            }
            currentParkour = generatedParkours[0];
            
        }
        else
        {
            Checkpoint tempCP = generatedParkours[generatedParkours.Count - 1].checkpoint;
            for (int i = 0; i < 3; i++)
            {
                Parkour parkour = Instantiate(GetRandomParkour(), parkourParent).GetComponent<Parkour>();
                parkour.transform.position = tempCP.transform.position;
                generatedParkours.Add(parkour);
                tempCP = parkour.checkpoint;
            }

            for (int i = 0; i < 3; i++)
            {
                Destroy(generatedParkours[0].gameObject);
                generatedParkours.RemoveAt(0);
                parkourGenIndex--;
            }
            
        }
    }

    GameObject GetRandomParkour()
    {
        if(parkourList.Count == 0)
        {
            parkourList = new List<GameObject>(randomParkours);
            Utility.ShuffleList(ref parkourList);
        }
        generate:
        int index = Random.Range(0, parkourList.Count);
        GameObject result = parkourList[index];
        if(parkourList[index] == lastParkourPrefab)
        {
            goto generate;
        }
        lastParkourPrefab = parkourList[index];
        parkourList.RemoveAt(index);
        return result;
    }

    IEnumerator SpawnParkourTask(GameObject parkourPrefab)
    {
        Parkour parkour = Instantiate(parkourPrefab, parkourParent).GetComponent<Parkour>();
        parkour.transform.position = currentCheckpoint.transform.position;
        currentParkour = parkour;
        yield return parkour.PopupObjectsTask();

    }

    public void OnBackButtonClick()
    {
        if(!paused)
        {
            closePanelButton.gameObject.SetActive(true);
            characterSelectionPanel.SetActive(true);
            fader.DOKill();
            fader.DOFade(0.5f, 0.3f);
            paused = true;
        }
        else
        {
            LoadScene("Episode4Map");
        }
    }

    public void OnClosePanelButtonClick()
    {
        paused = false;
        characterSelectionPanel.SetActive(false);
        fader.DOKill();
        fader.DOFade(0f, 0.3f);
    }

    public bool IsPaused()
    {
        return paused;
    }

    public void StartGame(int playerCount)
    {
        if(started)
        {
            started = false;
            restarted = true;
            restartPlayerCount = playerCount;
            LoadScene("WayBackHome");
        }
        else
        {
            started = true;
            StartCoroutine(StartGameTask(playerCount));
        }
        
    }

    public void PassNextParkour()
    {
        StartCoroutine(PassNextParkourTask());
    }

    IEnumerator PassNextParkourTask()
    {
        level += 1;
        checkPointTouch = 0;
        if(deathCounter == 0)
        {
            totalStars += 3;
        }
        else if(deathCounter == 1)
        {
            totalStars += 2;
        }
        else if(deathCounter == 2)
        {
            totalStars += 1;
        }
        starText.text = totalStars.ToString();
        deathCounter = 0;
        Destroy(currentCheckpoint.gameObject);
        currentCheckpoint = currentParkour.checkpoint;
        currentCheckpoint.transform.SetParent(null);
        //yield return currentParkour.PopDownObjectsTask();
        parkourGenIndex++;
        if(parkourGenIndex == generatedParkours.Count - 2)
        {
            GenerateNextParkours();
        }
        currentParkour = generatedParkours[parkourGenIndex];

        yield return null;
        // if(level >= 3)
        // {
        //     yield return SpawnParkourTask(GetRandomParkour());
        // }
        // else
        // {
        //     yield return SpawnParkourTask(starterParkours[level]);
        // }
        
    }

    IEnumerator StartGameTask(int playerCount)
    {
        for (int i = 0; i < playerCount; i++)
        {
            characters[i].gameObject.SetActive(true);
        }
        characterSelectionPanel.SetActive(false);
        fader.DOKill();
        fader.DOFade(0f, 0.3f);
        starPanel.SetActive(true);

        yield return new WaitForSeconds(1f);
        //yield return SpawnParkourTask(starterParkours[level]);

        if(playerCount == 2)
        {
            Camera.main.GetComponent<FollowMultiplayer>().enabled = true;
        }
        else
        {
            Camera.main.GetComponent<FollowSinglePlayer>().enabled = true;
        }
    }

    public bool IsMultiplayer()
    {
        return characters[1].gameObject.activeInHierarchy;
    }

    public void RespawnCharacter(int id)
    {
        deathCounter++;
        characters[id].SetPosition(currentCheckpoint.spawnPoints[id].position);
    }

    public void TouchCheckpoint(Checkpoint cp)
    {
        if(cp == currentParkour.checkpoint)
        {
            checkPointTouch++;
            if(AreCharactersArrivedCheckpoint())
            {
                PassNextParkour();
            }
        }
        
    }

    public void UnTouchCheckpoint(Checkpoint cp)
    {
        if(cp == currentParkour.checkpoint)
        {
            checkPointTouch--;
        }
    }

    public bool AreCharactersArrivedCheckpoint()
    {
        return IsMultiplayer() ? checkPointTouch == 2 : checkPointTouch == 1;
    }

    public void LoadScene(string sceneName)
	{
		if(passingAnotherScene)
		{
			return;
		}
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		passingAnotherScene = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen();
		SceneManager.LoadScene(sceneName);
	}
}
