﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningObject : MonoBehaviour
{
    [Header("Properties")]
    public float rotateSpeed;
    public bool isCircle = false;

    Rigidbody2D rigidbody2d;
    SurfaceEffector2D effector2D;
     
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        if(isCircle)
        {
            effector2D = transform.GetChild(0).GetComponent<SurfaceEffector2D>();
            if (rotateSpeed > 0)
            {
                effector2D.speed = -rotateSpeed / 100f;
            }
            else
            {
                effector2D.speed = rotateSpeed / 100f;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody2d.MoveRotation(rigidbody2d.rotation + rotateSpeed * Time.deltaTime);
        //transform.Rotate();
    }
}
