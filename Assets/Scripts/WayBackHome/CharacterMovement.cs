﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public BigMaxController controller;
    public ControllerDevice device;
    public float runSpeed;
    public int charId = 0;

    Rigidbody2D rigidbody2d;
    float horizontalMove = 0f;
    bool jump = false;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!WayBackHomeManager.Instance.IsPaused())
        {
            if(device == ControllerDevice.ARROWS)
            {
                horizontalMove = Input.GetAxisRaw("HorizontalArrows") * runSpeed;
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    jump = true;
                }
            }
            else if (device == ControllerDevice.WASD)
            {
                horizontalMove = Input.GetAxisRaw("HorizontalWASD") * runSpeed;
                if (Input.GetKeyDown(KeyCode.W))
                {
                    jump = true;
                }
            }
        }
        
        CheckForFall();
        
    }

    void CheckForFall()
    {
        if(rigidbody2d.velocity.y <= -30f)
        {
           Respawn();
        }
    }



    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    public void Respawn()
    {
         WayBackHomeManager.Instance.RespawnCharacter(charId);
    }

    public void SetPosition(Vector3 pos)
    {
        rigidbody2d.velocity = Vector3.zero;
        transform.position = pos;
    }

    
}

public enum ControllerDevice
{
    None, ARROWS, WASD
}
