﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Parkour : MonoBehaviour
{
    public Transform[] parkourObjects;
    public Checkpoint checkpoint;
    // Start is called before the first frame update
    void Awake()
    {
        // for (int i = 0; i < parkourObjects.Length; i++)
        // {
        //     parkourObjects[i].gameObject.SetActive(false);
        // }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator PopupObjectsTask()
    {
        for (int i = 0; i < parkourObjects.Length; i++)
        {
            parkourObjects[i].gameObject.SetActive(true);
            parkourObjects[i].localScale /= 2f;
            parkourObjects[i].DOScale(parkourObjects[i].localScale * 2f, 1f).SetEase(Ease.OutElastic);
            yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        }
    }

    public IEnumerator PopDownObjectsTask()
    {
        for (int i = 0; i < parkourObjects.Length; i++)
        {
            parkourObjects[i].DOScale(0f, 0.6f).SetEase(Ease.InBack);
        }
        yield return new WaitForSeconds(0.6f);
        Destroy(gameObject);
    }
}
