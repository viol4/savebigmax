﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BlinkObject : MonoBehaviour
{
    public float duration = 3f;
    public float passSpeed = 5f;


    Collider2D collider2d;
    SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        collider2d = GetComponentInChildren<Collider2D>();
        StartCoroutine(BlinkTask());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator BlinkTask()
    {
        while(true)
        {
            yield return new WaitForSeconds(duration);
            yield return sprite.DOFade(0f, passSpeed).SetSpeedBased().WaitForCompletion();
            collider2d.enabled = false;
            yield return new WaitForSeconds(duration);
            yield return sprite.DOFade(1f, passSpeed).SetSpeedBased().WaitForCompletion();
            collider2d.enabled = true;
        }
    }
}
