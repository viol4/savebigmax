﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FallingObject : MonoBehaviour
{
    public float duration = 3f;
    public float respawnDuration = 2f;

    Rigidbody2D rigidbody2d;
    bool falling = false;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator FallingTask()
    {
        Vector3 pos = transform.position;
        Vector3 rot = transform.eulerAngles;
        yield return transform.DOShakePosition(duration, Vector2.one * 0.05f, 20, 5, false, false).WaitForCompletion();
        rigidbody2d.bodyType = RigidbodyType2D.Dynamic;
        rigidbody2d.gravityScale = 2f;
        yield return new WaitForSeconds(respawnDuration);
        rigidbody2d.bodyType = RigidbodyType2D.Kinematic;
        rigidbody2d.gravityScale = 0f;

        transform.eulerAngles = rot;
        transform.position = pos;
        rigidbody2d.velocity = Vector2.zero;
        rigidbody2d.angularVelocity = 0f;
        falling = false;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(!falling && collision.gameObject.GetComponent<CharacterMovement>())
        {
            falling = true;
            StartCoroutine(FallingTask());
        }
        
    }
}
