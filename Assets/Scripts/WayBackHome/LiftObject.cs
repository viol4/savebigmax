﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LiftObject : MonoBehaviour
{
    [Header("Properties")]
    public float height = 2f;
    public float duration = 0.5f;

    Rigidbody2D rigidbody2d;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        StartCoroutine(LiftTask());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LiftTask()
    {
        while(true)
        {
            yield return rigidbody2d.DOMoveY(height, duration).SetRelative().SetEase(Ease.InOutSine).WaitForCompletion();
            yield return rigidbody2d.DOMoveY(-height, duration).SetRelative().SetEase(Ease.InOutSine).WaitForCompletion();
        }
    }
}
