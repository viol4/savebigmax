﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSinglePlayer : MonoBehaviour
{
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target)
        {
            Vector3 pos = transform.position;
            pos = Vector3.Lerp(pos, target.position, 3f * Time.deltaTime);
            pos.z = -10f;
            transform.position = pos;
        }
    }
}
