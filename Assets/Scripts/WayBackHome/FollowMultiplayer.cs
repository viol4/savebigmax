﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMultiplayer : MonoBehaviour
{
    public Transform[] players;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FixedCameraFollowSmooth(Camera.main, players[0], players[1]);
    }

    public void FixedCameraFollowSmooth(Camera cam, Transform t1, Transform t2)
    {
     // How many units should we keep from the players
     float followTimeDelta = 0.1f;
 
     // Midpoint we're after
     Vector3 midpoint = (t1.position + t2.position + Vector3.up * 3f) / 2f;
    midpoint.z = -10f;
    
     // Distance between objects
     float distance = (t1.position - t2.position).magnitude * 1.1f;
    // if((Mathf.Abs(t1.position.y - t2.position.y) > 5f))
    // {
    //     midpoint += Vector3.up * 5f;
    // }
        //midpoint += Vector3.up * 5 * (cam.orthographicSize - 5f) / 5f;
     // Move camera a certain distance

     // Adjust ortho size if we're using one of those
     if (cam.orthographic)
     {
         // The camera's forward vector is irrelevant, only this size will matter
         cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distance, 1f * Time.deltaTime);
         cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, 6f, 11f);
     }
     // You specified to use MoveTowards instead of Slerp
     cam.transform.position = Vector3.Slerp(cam.transform.position, midpoint, followTimeDelta);
         
     // Snap when close enough to prevent annoying slerp behavior
     if ((midpoint - cam.transform.position).magnitude <= 0.05f)
         cam.transform.position = midpoint;

    }

    void LateUpdate()
    {
        Vector3 pos = Camera.main.transform.position;
        pos.x = Mathf.Max(-5.6f, pos.x);
        pos.y = Mathf.Max(-5.6f, pos.y);
        Camera.main.transform.position = pos;
    }
}
