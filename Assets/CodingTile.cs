﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CodingTile : MonoBehaviour
{
    public SpriteRenderer sprite;
    public SpriteRenderer actionSprite;
    [Space]
    [Header("Move Options")]
    public MoveActionType correctMoveAction;
    [Header("Turn Options")]
    public TurnActionType[] correctTurnActions;
    [Header("Obstacle Options")]
    public bool isObstacleAction;
    public ObstacleActionType obstacleActionType;
    public CodingTile occupationTile;
    public CodingTile finalTile;
    [Space]
    public bool occupiedByObstacle;
    public SpriteRenderer obstacleSprite;
    
    

    MoveActionType currentMove = MoveActionType.None;
    TurnActionType currentTurn = TurnActionType.None;
    ObstacleActionType currentObstacle = ObstacleActionType.None;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void RotateActionSprite(float angle)
    {
        actionSprite.transform.eulerAngles = new Vector3(0f, 0f, angle);
    }

    public void FadeOut()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        ResetAction();
        sprite.DOFade(0f, 0.5f);
        if(occupiedByObstacle)
        {
            obstacleSprite.DOFade(0f, 0.5f);
        }
    }

    public CodingItemType GetActionType()
    {
        if(currentMove != MoveActionType.None)
        {
            return CodingItemType.Move;
        }
        else if(currentTurn != TurnActionType.None)
        {
            return CodingItemType.Turn;
        }
        else  if(currentObstacle != ObstacleActionType.None)
        {
            return CodingItemType.Obstacle;
        }
        return CodingItemType.None;
    }

    public MoveActionType GetMoveAction()
    {
        return currentMove;
    }

    public TurnActionType GetTurnAction()
    {
        return currentTurn;
    }

    public ObstacleActionType GetObstacleAction()
    {
        return currentObstacle;
    }

    public void ResetAction()
    {
        currentMove = MoveActionType.None;
        currentTurn = TurnActionType.None;
        currentObstacle = ObstacleActionType.None;
        actionSprite.gameObject.SetActive(false);
    }

    public void SetAction(MoveActionType moveAction)
    {
        ResetAction();
        currentMove = moveAction;
        actionSprite.sprite = CodeOptionManager.Instance.moveSprites[(int)moveAction - 1];
        if(moveAction == MoveActionType.Up)
        {
            RotateActionSprite(90f);
        }
        else if(moveAction == MoveActionType.Down)
        {
            RotateActionSprite(270f);
        }
        else if(moveAction == MoveActionType.Left)
        {
            RotateActionSprite(180f);
        }
        else if(moveAction == MoveActionType.Up)
        {
            RotateActionSprite(0f);
        }
        actionSprite.gameObject.SetActive(true);
    }

    public void SetAction(TurnActionType turnAction)
    {
        ResetAction();
        currentTurn = turnAction;
        actionSprite.sprite = CodeOptionManager.Instance.turnSprites[(int)turnAction - 1];
        RotateActionSprite(0f);
        actionSprite.gameObject.SetActive(true);
    }

    public void SetAction(ObstacleActionType obstacleType)
    {
        ResetAction();
        currentObstacle = obstacleType;
        actionSprite.sprite = CodeOptionManager.Instance.obstacleSprites[(int)obstacleType - 1];
        RotateActionSprite(0f);
        actionSprite.gameObject.SetActive(true);
    }

    public void CheckForAvailableMoveActions()
    {
        List<MoveActionType> moveActions = new List<MoveActionType>();
        //CodingTile[] neighbors = new CodingTile[4];
        // neighbors[0] = Utility.getColliderAt<CodingTile>(Utility.mouseToWorldPosition() + Vector3.up * CodingManager.Instance.tileSpaceY);
        // neighbors[1] = Utility.getColliderAt<CodingTile>(Utility.mouseToWorldPosition() + Vector3.down * CodingManager.Instance.tileSpaceY);
        // neighbors[2] = Utility.getColliderAt<CodingTile>(Utility.mouseToWorldPosition() + Vector3.left * CodingManager.Instance.tileSpaceX);
        // neighbors[3] = Utility.getColliderAt<CodingTile>(Utility.mouseToWorldPosition() + Vector3.right * CodingManager.Instance.tileSpaceX);
        
        moveActions.Add(correctMoveAction);
        // moveActions.Add(MoveActionType.Left);
        // moveActions.Add(MoveActionType.Up);
        // moveActions.Add(MoveActionType.Down);
    
        // if(neighbors[2] && neighbors[3])
        // {
            
        // }

        
        // if(neighbors[0] && neighbors[1])
        // {
            
        // }
        CodeOptionManager.Instance.SetOptions(this, moveActions);
    }

    public void CheckForAvailableTurnActions()
    {
        CodeOptionManager.Instance.SetOptions(this, new List<TurnActionType>(correctTurnActions));
    }

    public void SetObstacleAction(int index)
    {
        SetAction((ObstacleActionType)index);
    }
}
